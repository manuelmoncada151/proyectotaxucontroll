<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class Status
	{
		private $ID_ESTADO;
		private $NOM_ESTADO;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM estado ";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getStatusById($ID_ROL)
		{
			try {
				$strSql = "SELECT * FROM estado WHERE ID_ESTADO = :id";
				$arrayData = ['id' => $ID_ESTADO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		
		public function newStatus($data)
		{
			try {
				$this->pdo->insert('estado', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editStatus($data)
		{
			try {
				$strWhere = 'ID_ESTADO = '. $data['ID_ESTADO'];
				$this->pdo->update('estado', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteStatus($data)
		{
			try {
				$strWhere = 'ID_ESTADO = '. $data['id'];
				$this->pdo->delete('estado', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}


	}