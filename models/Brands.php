<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class Brand
	{
		private $ID_MARCA;
		private $NOM_MARCA;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM marca";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
			public function getBrandById($ID_MARCA)
		{
			try {
				$strSql = "SELECT * FROM marca WHERE ID_MARCA = :id";
				$arrayData = ['id' => $ID_MARCA];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		
		public function newBrand($data)
		{
			try {
				
				$this->pdo->insert('marca', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		

		public function editBrand($data)
		{
			try {
				$strWhere = 'ID_MARCA = '. $data['ID_MARCA'];
				$this->pdo->update('marca', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteBrand($data)
		{
			try {
				$strWhere = 'ID_MARCA = '. $data['ID_MARCA'];
				$this->pdo->delete('marca', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
	}