<?php
	
	/**
	 * Modelo de la Tabla person
	 */
	class Person
	{
		private $ID_PERSONA;
		private $DOCU_PERSONA;
		private $NOM_PERSONA;
		private $TEL_PERSONA;
		private $CORREO_PERSONA;
        private $DIREC_PERSONA;
        private $ID_USUARIO_FK;
        private $ID_ROL_FK;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT p.*,u.NOM_USUARIO as user, r.NOM_ROL as rol FROM persona p INNER JOIN usuario u ON u.ID_USUARIO = p.ID_USUARIO_FK INNER JOIN rol r ON r.ID_ROL = p.ID_ROL_FK";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
        }
        
		public function getActivePersons()
		{
			try {
				$user=$_SESSION['user']->ID_USUARIO;
				$strSql = "SELECT p.*,s.NOM_ESTADO as status, CONCAT(p.NOM_PERSONA,' ',p.APE_PERSONA) as nom FROM persona p INNER JOIN usuario u ON u.ID_USUARIO = p.ID_USUARIO_FK 
							INNER JOIN rol r ON r.ID_ROL = u.ID_ROL_FK
							inner join estado s on s.ID_ESTADO=u.ID_ESTADO_FK where u.ID_ESTADO_FK = 1 and u.ID_ROL_FK = 3 ";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		public function newPerson($data)
		{
			try {
				$data['ID_ESTADO_FK'] = 1;
				$this->pdo->insert('persona', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getPersonById($ID_PERSONA)
		{
			try {
				$strSql = "SELECT * FROM persona WHERE ID_PERSONA = :id";
				$arrayData = ['id' => $ID_PERSONA];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		// public function editUser($data)
		// {
		// 	try {
		// 		$strWhere = 'ID_USERS ='. $data['ID_USERS'];
		// 		$this->pdo->update('users', $data, $strWhere);				
		// 	} catch(PDOException $e) {
		// 		die($e->getMessage());
		// 	}		
		// }

		// public function deleteUser($data)
		// {
		// 	try {
		// 		$strWhere = 'ID_USERS = '. $data['id'];
		// 		$this->pdo->delete('users', $strWhere);
		// 	} catch(PDOException $e) {
		// 		die($e->getMessage());
		// 	}	
        // }
        
		public function editStatus($data)
		{
			try {
				$strWhere = 'ID_NOVEDAD = '. $data['ID_NOVEDAD'];
				$this->pdo->update('novedad',$data, $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

	}