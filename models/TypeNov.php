<?php
	
	/**
	 * Modelo de la Tabla type_novelty
	 */
	class TypeNov
	{
		private $ID_TIPONOV;
		private $NOM_TIPONOV;
		private $ID_CLASINOV_FK;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT t.*,c.NOM_CLASINOV as classif FROM tipo_novedad t INNER JOIN clasificacion_novedad c ON c.ID_CLASINOV = t.ID_CLASINOV_FK order by t.ID_TIPONOV";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
        }
        
        public function getTypeNovById($ID_TIPONOV)
		{
			try {
				$strSql = "SELECT * FROM tipo_novedad WHERE ID_TIPONOV = :id";
				$arrayData = ['id' => $ID_TIPONOV];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		
		public function newTypeNov($data)
		{
			try {
				$this->pdo->insert('tipo_novedad', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editTypeNov($data)
		{
			try {
				$strWhere = 'ID_TIPONOV = '. $data['ID_TIPONOV'];
				$this->pdo->update('tipo_novedad', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteTypeNov($data)
		{
			try {
				$strWhere = 'ID_TIPONOV = '. $data['id'];
				$this->pdo->delete('tipo_novedad', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

	}