<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class Novelty
	{
		private $ID_NOVEDAD;
		private $DESCRIP_NOVEDAD;
		private $FECH_NOVEDAD;
		private $URL_SUPNOV;
		private $VALOR_NOVEDAD;
        private $ID_PERSONA_FK;
        private $ID_CLASINOV_FK;
        private $ID_ESTADO_FK;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT n.*,s.NOM_ESTADO as status, CONCAT(p.NOM_PERSONA,' ',p.APE_PERSONA) as person, c.NOM_CLASINOV as classif, v.ID_VEHICULO as vehiculo,v.PLACA_VEHICULO as vehi, v.ID_ESTADO_FK as estveh
							FROM novedad n 
							INNER JOIN estado s ON s.ID_ESTADO = n.ID_ESTADO_FK 
                            INNER JOIN persona p ON p.ID_PERSONA = n.ID_PERSONA_FK 
                            INNER JOIN convenio_producido co ON co.ID_PERSONA_FK = p.ID_PERSONA
                            INNER JOIN vehiculo v ON v.PLACA_VEHICULO = co.PLACA_VEHICULO_FK
                            INNER JOIN estado se ON se.ID_ESTADO = v.ID_ESTADO_FK 
                            INNER JOIN clasificacion_novedad c ON c.ID_CLASINOV=n.ID_CLASINOV_FK 
                            INNER JOIN usuario U ON U.ID_USUARIO=p.ID_USUARIO_FK order by n.ID_NOVEDAD";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}
		 public function getActivePersons()
		 {
		 	try {
		 		$user=$_SESSION['user']->ID_USUARIO;
		 		$strSql = "SELECT p.*,s.NOM_ESTADO as status, CONCAT(p.NOM_PERSONA,' ',p.APE_PERSONA) as nom FROM persona p 
							INNER JOIN rol r ON r.ID_ROL = u.ID_ROL_FK
							inner join estado s on s.ID_ESTADO=u.ID_ESTADO_FK 
							INNER JOIN usuario u ON u.ID_USUARIO = p.ID_USUARIO_FK 
							where u.ID_ESTADO_FK = 1 and u.ID_ROL_FK = 3 and ID_USUARIO = $user; ";
		 		//Llamado al metodo general que ejecuta un select a la BD
		 		$query = $this->pdo->select($strSql);
		 		//retorna el objeto del query
		 		return $query;
		 	} catch(PDOException $e) {
		 		die($e->getMessage());
		 	}
		}
		public function newNovelty($data)
		{
			try {
				$data['ID_ESTADO_FK'] = 3;
				$this->pdo->insert('novedad', $data);	
				return true;			
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getAllById()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT n.*,s.NOM_ESTADO as status, CONCAT(p.NOM_PERSONA,' ',p.APE_PERSONA) as person, 
            					c.NOM_CLASINOV as classif, v.ID_VEHICULO as vehiculo,v.PLACA_VEHICULO as vehi, v.ID_ESTADO_FK as estveh FROM novedad n 
            					INNER JOIN estado s ON s.ID_ESTADO = n.ID_ESTADO_FK 
            					INNER JOIN persona p ON p.ID_PERSONA = n.ID_PERSONA_FK 
            					INNER JOIN convenio_producido co ON co.ID_PERSONA_FK = p.ID_PERSONA
                            	INNER JOIN vehiculo v ON v.PLACA_VEHICULO = co.PLACA_VEHICULO_FK 
                            	INNER JOIN clasificacion_novedad c ON c.ID_CLASINOV=n.ID_CLASINOV_FK 
                            	INNER JOIN usuario U ON U.ID_USUARIO=p.ID_USUARIO_FK WHERE ID_USUARIO=$user";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

		public function getNoveltyById($ID_NOVEDAD)
		{
			try {
				$strSql = "SELECT * FROM novedad WHERE ID_NOVEDAD = :id";
				$arrayData = ['id' => $ID_NOVEDAD];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getLastId()
		{
			try {
				$strSql = "SELECT MAX(ID_NOVEDAD) as id FROM novedad";
				$query = $this->pdo->select($strSql);
				return $query; 
			} catch(PDOException $e) {
				return$e->getMessage();
			}	
		}

		public function saveDetNovelty($arrayTypes, $lastIdNovelty)
		{
			try {
				
				foreach ($arrayTypes as $types) {
					$data = [
						'ID_TIPONOV_FK' => $types['id'],
						'ID_NOVEDAD_FK' => $lastIdNovelty
						
					];
				$this->pdo->insert('detalle_novedad', $data);	
				}
				return true;				
			} catch(PDOException $e) {
				return$e->getMessage();
			}	
		}

		public function deleteDetNovelty($id)
		{
			try {
				$strWhere = 'ID_NOVEDAD_FK = '. $id;
				$this->pdo->delete('detalle_novedad', $strWhere);
				return true;
			} catch(PDOException $e) {
				return$e->getMessage();
			}
		}

		public function editStatus($data)
		{
			try {
				$strWhere = 'ID_NOVEDAD = '. $data['ID_NOVEDAD'];
				$this->pdo->update('novedad',$data, $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getVehById($ID_VEHICULO)
		{
			try {
				$strSql = "SELECT * FROM vehiculo WHERE ID_VEHICULO = :id";
				$arrayData = ['id' => $ID_VEHICULO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editVeh($data)
		{
			try {
			
				$strWhere = 'ID_VEHICULO = '. $data['ID_VEHICULO'];
				$this->pdo->update('vehiculo', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function UpdateVeh($dataVeh)
		{
			try {
				$check = $dataVeh['ID_ESTADO_FK'];
				$placa = $dataVeh['vehiculo'];
				
				if($check === "true"){
					$link = mysqli_connect("localhost", "root", '', "taxucontroll");
			    // Chequea coneccion
			    if($link === false){
			        die("ERROR: No pudo conectarse con la DB. " . mysqli_connect_error());
			    }
			    // Ejecuta la actualizacion del registro
			    $sql = "UPDATE vehiculo SET ID_ESTADO_FK = 2 WHERE PLACA_VEHICULO='$placa'";
			    if(mysqli_query($link, $sql)){
			        //echo "Registro actualizado.";
			    } else {
			        echo "ERROR: No se ejecuto $sql. " . mysqli_error($link);
			        die();
			    }
			    // Cierra la conexion
			    mysqli_close($link);
			    return true;
			    
				}else if($check === "false"){
					$link = mysqli_connect("localhost", "root", '', "taxucontroll");
			    // Chequea coneccion
			    if($link === false){
			        die("ERROR: No pudo conectarse con la DB. " . mysqli_connect_error());
			    }
			    // Ejecuta la actualizacion del registro
			    $sql = "UPDATE vehiculo SET ID_ESTADO_FK = 1 WHERE PLACA_VEHICULO='$placa'";
			    if(mysqli_query($link, $sql)){
			        //echo "Registro actualizado.";
			    } else {
			        echo "ERROR: No se ejecuto $sql. " . mysqli_error($link);
			        die();
			    }
			    // Cierra la conexion
			    mysqli_close($link);
			    return true;
				}			
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function getVeh()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql = "SELECT co.*,v.ID_VEHICULO as vehiculo, v.PLACA_VEHICULO as veh
							FROM convenio_producido co
                            INNER JOIN persona p ON p.ID_PERSONA = co.ID_PERSONA_FK 
                            INNER JOIN vehiculo v ON v.PLACA_VEHICULO = co.PLACA_VEHICULO_FK
                            INNER JOIN estado se ON se.ID_ESTADO = v.ID_ESTADO_FK 
                            INNER JOIN usuario U ON U.ID_USUARIO=p.ID_USUARIO_FK
                            where p.ID_USUARIO_FK=$user";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

	}