<?php
	
	/**
	 * Modelo de la Tabla person
	 */
	
	class Role
	{
		private $ID_ROL;
		private $NOM_ROL;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM rol";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
        }

        public function getRoleById($ID_ROL)
		{
			try {
				$strSql = "SELECT * FROM rol WHERE ID_ROL = :id";
				$arrayData = ['id' => $ID_ROL];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		
		public function newRole($data)
		{
			try {
				$this->pdo->insert('rol', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editRole($data)
		{
			try {
				$strWhere = 'ID_ROL = '. $data['ID_ROL'];
				$this->pdo->update('rol', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteRole($data)
		{
			try {
				$strWhere = 'ID_ROL = '. $data['id'];
				$this->pdo->delete('rol', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

	}