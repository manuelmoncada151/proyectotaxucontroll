<?php
	
	/**
	 * Modelo de la Tabla person
	 */
	
	class Classif
	{
		private $ID_CLASINOV;
		private $NOM_CLASINOV;
		private $DESCRIP_CLASINOV;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM clasificacion_novedad";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
        }

        public function getClassifById($ID_CLASINOV)
		{
			try {
				$strSql = "SELECT * FROM clasificacion_novedad WHERE ID_CLASINOV = :id";
				$arrayData = ['id' => $ID_CLASINOV];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
		
		public function newClassif($data)
		{
			try {
				$this->pdo->insert('clasificacion_novedad', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editClassif($data)
		{
			try {
				$strWhere = 'ID_CLASINOV = '. $data['ID_CLASINOV'];
				$this->pdo->update('clasificacion_novedad', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteClassif($data)
		{
			try {
				$strWhere = 'ID_CLASINOV = '. $data['id'];
				$this->pdo->delete('clasificacion_novedad', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

	}