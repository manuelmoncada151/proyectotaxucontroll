<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class Ea
	{
		private $ID_ENTREAHORRO;
		private $VALOR_ENTREAHORRO;
		private $FECHAINICIO_CONVENIO;
		private $FECHAFIN_CONVENIO;
		private $ID_CONVENIO_FK;
		
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT ea. *, c.MEDIDA_CONVENIO as MEDIDA_CONVENIO from entrega_ahorro ea INNER JOIN CONVENIO_PRODUCIDO c ON c.ID_CONVENIO=ea.ID_CONVENIO_FK 
				ORDER BY ea.ID_ENTREAHORRO";

				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function newEa($data)
		{
			try {
				$this->pdo->insert('entrega_ahorro', $data);
				return true;				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getEaById($ID_ENTREAHORRO)
		{
			try {
				$strSql = "SELECT * FROM entrega_ahorro WHERE ID_ENTREAHORRO = :id";
				$arrayData = ['id' => $ID_ENTREAHORRO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editEa($data)
		{
			try {
			
				$strWhere = 'ID_ENTREAHORRO = '. $data['ID_ENTREAHORRO'];
				$this->pdo->update('entrega_ahorro', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteEa($data)
		{
			try {
				$strWhere = 'ID_ENTREAHORRO = '. $data['ID_ENTREAHORRO'];
				$this->pdo->delete('entrega_ahorro', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
	}