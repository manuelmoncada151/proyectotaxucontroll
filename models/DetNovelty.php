<?php
	
	/**
	 * Modelo de la Tabla users
	 */
	class DetNovelty
	{
		private $ID_DETNOV;
		private $ID_TIPONOV_FK;
		private $ID_NOVEDAD_FK;
		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$id = $_REQUEST['id'];
				$strSql = "SELECT dn.*,t.NOM_TIPONOV as type FROM detalle_novedad dn INNER JOIN tipo_novedad t ON t.ID_TIPONOV = dn.ID_TIPONOV_FK where ID_NOVEDAD_FK=$id ORDER BY dn.ID_DETNOV";
				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}


	}