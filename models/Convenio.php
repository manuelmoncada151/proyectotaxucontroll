<?php
/**
 *
 */
class Convenio
{
    private $pdo;
    public function __construct()
    {
        try {
            $this->pdo = new Database;

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT c.*,p.NOM_PERSONA as nombre,p.APE_PERSONA as apellido, u.ID_USUARIO as usuario FROM convenio_producido c INNER JOIN persona p on p.ID_PERSONA=c.ID_PERSONA_FK INNER JOIN usuario u on u.ID_USUARIO=p.ID_PERSONA";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }   

     public function promedio()
    {
        try {
            $strSql = "SELECT TRUNCATE(AVG(co.VALOR_CONVENIO),0) as promedio FROM convenio_producido co";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function Count()
    {
        try {
            $strSql = "SELECT COUNT(*) AS NUM FROM persona WHERE ID_ROL_FK=3";
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getConvenioUser()
    {
        try {
            $user=$_SESSION['user']->ID_USUARIO;
            $strSql="SELECT ID_CONVENIO,FECHAINICIO_CONVENIO AS FECHA,CONCAT(NOM_PERSONA,' ', APE_PERSONA)AS NOMBRE FROM convenio_producido c
            INNER JOIN persona p on p.ID_PERSONA=c.ID_PERSONA_FK
            INNER JOIN usuario u on u.ID_USUARIO=p.ID_USUARIO_FK
            WHERE u.ID_USUARIO=$user";
           
            //Llamado al metodo general que ejecuta un select a la BD
            $query = $this->pdo->select($strSql);
            //retorna el objeto del query

            return $query;

        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function newConvenio($data)
    {
        try {
            $data['S_N_CANCELADO'] = 2;
            //$data['PASWORD']= hash('256', $data['PASWORD']);
            $this->pdo->insert('convenio_producido', $data);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function getConvenioById($id)
    {
        try {
            $strSql    = "SELECT * FROM convenio_producido WHERE ID_CONVENIO = :id";
            $arrayData = ['id' => $id];
            $query     = $this->pdo->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function editConvenio($data)
    {
        try {

            $strWhere = 'ID_CONVENIO = ' . $data["ID_CONVENIO"];
            $this->pdo->update('convenio_producido', $data, $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
    public function deleteConvenio($data)
    {
        try {
            var_dump($data);
            $strWhere = 'ID_CONVENIO = ' . $data['id'];
            $this->pdo->delete('convenio_producido', $strWhere);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

}
