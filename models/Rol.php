<?php
	
	/**
	 * Modelo de la Tabla rol
	 */
	class Rol
	{
		
		
		private $ID_ROL;
		private $NOM_ROL;

		private $pdo;
		
		public function __construct()
		{
			try {
				$this->pdo = new Database;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function getAll()
		{
			try {
				$strSql = "SELECT * FROM rol";

				//Llamado al metodo general que ejecuta un select a la BD
				$query = $this->pdo->select($strSql);
				//retorna el objeto del query
				return $query;
			} catch(PDOException $e) {
				die($e->getMessage());
			}
		}

		public function newVeh($data)
		{
			try {
				$data['ID_ESTADO_FK'] = 1;
		
				$this->pdo->insert('vehiculo', $data);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function getUserById($ID_USUARIO)
		{
			try {
				$strSql = "SELECT * FROM usuario WHERE ID_USUARIO = :id";
				$arrayData = ['id' => $ID_USUARIO];
				$query = $this->pdo->select($strSql, $arrayData);
				return $query; 
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}

		public function editUser($data)
		{
			try {
			
				$strWhere = 'ID_USUARIO = '. $data['ID_USUARIO'];
				$this->pdo->update('usuario', $data, $strWhere);				
			} catch(PDOException $e) {
				die($e->getMessage());
			}		
		}

		public function deleteUser($data)
		{
			try {
				$strWhere = 'ID_USUARIO = '. $data['id'];
				$this->pdo->delete('usuario', $strWhere);
			} catch(PDOException $e) {
				die($e->getMessage());
			}	
		}
	}