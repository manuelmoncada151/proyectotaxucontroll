$(document).ready(function(){
  $.ajax({
    type: 'POST',
    url: 'views/novelty/select/cargar_classif.php'
  })
  .done(function(listas_rep){
    $('#ID_CLASINOV_FK').html(listas_rep)
  })
  .fail(function(){
    alert('Hubo un errror al cargar las listas_rep')
   })

  $('#ID_CLASINOV_FK').on('change', function(){
    var id = $('#ID_CLASINOV_FK').val()
    $.ajax({
      type: 'POST',
      url: 'views/novelty/select/cargar_tipos.php',
      data: {'id': id}
    })
    .done(function(listas_rep){
      $('#tipos').html(listas_rep)
    })
    .fail(function(){
      alert('Hubo un errror al cargar los vídeos')
    })
  })

  $('#enviar').on('click', function(){
        var resultado = 'Lista de reproducción: ' + $('#ID_CLASINOV_FK option:selected').text() +
    ' Video elegido: ' + $('#tipos option:selected').text()
      console.log(resultado);
    $('#resultado1').html(resultado)
  })

}) 