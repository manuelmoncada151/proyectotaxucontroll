<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Taxucontroll | Inicio</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }

    </script>
    <link rel="icon" href="assets/images/taxicon.png" type="image/ico" />
    <!-- Custom-Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all" />
    <!-- Style-CSS -->
    <!-- font-awesome-icons -->
    <link href="assets/css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Khula:300,400,600,700,800" rel="stylesheet">
    <!-- //Fonts -->
</head>

<body>
    <!-- mian-content -->
    <div class="main-content" id="home">
        <div class="layer">
            <!-- header -->
            <header>
                <div class="container-fluid px-lg-5">
                    <!-- nav -->
                    <nav class="py-4 d-lg-flex">
                        <div id="logo">
                            <img src="assets/images/tax8.png" class="logo-brand" alt="logo">
                            <label for="drop" class="toggle">Menu</label>   
                        </div>
                        <input type="checkbox" id="drop" />
                        <ul class="menu mt-2 ml-auto">

                            <li>
                                <!-- First Tier Drop Down -->
                                
                                <a href="#about">Nosotros<span class="fa" aria-hidden="true"></span></a>
                                <input type="checkbox" id="drop-2" />
                                
                                <li><a href="#team" class="scroll">Sociedad</a></li>
                                <li><a href="#menú" class="scroll">Beneficios</a></li>
                                <li><a href="#testimonials" class="scroll">Testimonios</a></li>

                            </li>
                            <li><a href="#contact" class="scroll">Contáctenos</a></li>
                            <li><a href="Start.php" class="scroll">Iniciar Sesión</a></li>
                        </ul>
                    </nav>
                    <!-- //nav -->
                </div>
            </header>
            <!-- //header -->
            <div class="container">
                <!-- /banner -->
                <div class="banner-info-w3ls text-left">
                    <h5 style="font-size: 50px;"> El mejor software en línea para gestionar su compañía de taxis y taxistas.</h5>
                    <div class="read-more">
                        <a href="#services" class="read-more scroll btn">Conócenos</a> </div>
                    </div>
                    <div class="row order-info">
                        <div class="middle mt-md-3 col-sm-6 text-left">

                        </div>
                        <div class="middle-right mt-md-3 col-sm-6 text-sm-right">
                        </div>
                    </div>
                </div>
                <!-- //banner -->
            </div>
        </div>
        <!--// mian-content -->
        <!-- banner-bottom-w3layouts -->
        <section class="banner-bottom-w3layouts py-5" id="about">
            <div class="container py-md-3">
                <div class="row banner-grids">
                    <div class="form-group row">
                        <div class="col-md-6 content-left-bottom text-left pr-lg-5">
                            <h3>Taxucontroll</h3>
                            <p class="mt-2 text-left">Es la solución tecnológica mas eficaz al alcance de tu bolsillo<strong class="text-capitalize"> Taxucontroll es quien te quitará los problemas contables que presentes</strong> Funciona en todos los dispositivos móviles y lo mejor ¡Está en la web!
                                Con nosotros puedes tener control de tus vehículos, novedades, conductores y socios dentro de tu compañía, Puedes controlar pagos, aportes y validarlos dentro del sistema<strong style="font-size: 20px;"> <p>¡No lo pienses más usa Taxucontroll!</strong></p>

                            </div>
                            <div class="col-md-4 content-right-bottom text-left">
                                <img style="padding-top: -50px" src="assets/images/ab.png" alt="news image" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- //banner-bottom-w3layouts -->
            <!--/ about -->
            <section class="services py-5" id="services">
                <div class="container py-md-5">
                    <div class="header pb-lg-3 pb-3 text-center">
                        <h3 class="tittle mb-lg-3 mb-3">Que ofrecemos...</h3>
                    </div>
                    <div class="row">
                        <div class="row ab-info mt-lg-4">
                            <div class="col-lg-3 ab-content">
                                <div class="ab-content-inner">
                                    <div class="ab-info-con">
                                        <div class="ab-icon mx-auto"><span class="fa fa-car" aria-hidden="true"></span></div>
                                        <h4>Control sobre sus vehículos</h4>
                                        <p>Control total en la asignación de vehículos.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 ab-content">
                                <div class="ab-content-inner">
                                    <div class="ab-info-con">
                                        <div class="ab-icon mx-auto"><span class="fa fa-bell-o" aria-hidden="true"></span></div>
                                        <h4>Sistema de novedades</h4>
                                        <p>Creación y reporte de novedades en tiempo real.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 ab-content">
                                <div class="ab-content-inner">
                                    <div class="ab-info-con">
                                        <div class="ab-icon mx-auto"><span class="fa  fa-money" aria-hidden="true"></span></div>
                                        <h4>Sistema de pagos</h4>
                                        <p>Creación de reporte de pagos según el acuerdo con tus conductores.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 ab-content">
                                <div class="ab-content-inner">
                                    <div class="ab-info-con">
                                        <div class="ab-icon mx-auto"><span class="fa fa-close" aria-hidden="true"></span></div>
                                        <h4>Sin errores contables</h4>
                                        <p>Las operaciones son efectivas y rápidas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--// about -->
            <!--/mid-sec-->
            <section class="mid-sec py-5" id="menú">
                <div class="container py-lg-5">
                    <div class="header pb-lg-3 pb-3 text-center">
                        <h3 class="tittle mb-lg-3 mb-3">Beneficios</h3>
                    </div>
                    <div class="row middile-inner-con mt-md-3">
                        <div class="col-lg-4 benifit-content">
                            <div class="row form-group">
                                <div class="col-md-8 benifit-left-info text-right">
                                    <h4 class="mb-3">Navegación eficaz</h4>
                                    <p>Fácil, rápido y sencillo de usar para cualquier persona.</p>
                                </div>
                                <div class="col-md-4 benifit-icon-content">
                                    <div class="benifit-icon">
                                        <span class="fa fa-plane" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-4">
                                <div class="col-md-8 benifit-left-info text-right">
                                    <h4 class="mb-3">Seguridad</h4>
                                    <p>Sistema robusto frente posibles ataques.</p>
                                </div>
                                <div class="col-md-4 benifit-icon-content">
                                    <div class="benifit-icon">
                                        <span class="fa fa-unlock-alt" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 benifit-left-info text-right">
                                    <h4 class="mb-3">Control de usuario</h4>
                                    <p>Administra las necesidades según el usuario.</p>
                                </div>
                                <div class="col-md-4 benifit-icon-content">
                                    <div class="benifit-icon">
                                        <span class="fa fa-puzzle-piece" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 benifit-content">
                            <img src="assets/images/icon1.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="col-lg-4 benifit-content">
                            <div class="row">
                                <div class="col-md-4 benifit-icon-content">
                                    <div class="benifit-icon text-md-center mx-auto">
                                        <span class="fa fa-group" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-md-8 benifit-left-info text-left">
                                    <h4 class="mb-3">Funcionalidad</h4>
                                    <p>El sistema automatiza todas las funciones de su empresa.</p>
                                </div>
                            </div>

                            <div class="row  my-4">
                                <div class="col-md-4 benifit-icon-content">
                                    <div class="benifit-icon text-md-center mx-auto">
                                        <span class="fa fa-taxi" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-md-8 benifit-left-info text-left">
                                    <h4 class="mb-3">Diseño apropiado</h4>
                                    <p>Agradable a la vista y personalizado para mayor comodidad.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 benifit-icon-content">
                                    <div class="benifit-icon text-md-center mx-auto">
                                        <span class="fa fa-taxi" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-md-8 benifit-left-info text-left">
                                    <h4 class="mb-3">Usabilidad</h4>
                                    <p>Brinda facilidades de uso.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
            <!--//mid-sec-->

            <!--/ banner-bottom-w3layouts-->
            <section class="app-serve pt-3 " style="height: 300px">
                <div class="container-fluid pt-lg-3">
                    <div class="ab-grids row text-left">
                        <div class="col-lg-4 pl-0 pr-lg-5 mt-lg-5 app-mob">
                            <img src="assets/images/app1.png"  class="img-fluid" alt="mobile-image" height="500px" width="400px">
                        </div>
                        <div class="col-lg-6 mb-lg-5">

                            <h3 class="tittle mb-lg-5 mb-3" >Taxucontroll en tus manos</h3>


                            <div class="feature-grids text-left mt-5 p-lg-5">
                                <div class="bottom-gd  ">
                                    <div class="col-md-2">       
                                    </div>
                                    <h3 class="my-4" style="font-size: 28px;">Sin costos adicionales y accesible a tu bolsillo</h3>
                                </div>
                            </div>
                            <p style="font-size: 20px;">¡Conéctate y disfruta!</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// banner-bottom-w3layouts-->

    <!-- Gallery -->
    <section class="gallery py-5" id="team">
        <div class="container py-md-5">
            <div class="header text-center">
                <h3 class="tittle mb-lg-5 mb-3">Sociedad</h3>
            </div>
            <div class="row team-bottom">
                <div class="col-lg-3 col-sm-6 team-grid text-center">
                    <img src="assets/images/David.jpeg" style="height: 270px; width: 400px" class="img-fluid" alt="">
                    <div class="caption">
                        <h4>David Palacios</h4>
                        <ul class="list-unstyled w3pvt-icons">
                            <li>
                                <a href="#">
                                    <span class="fa fa-facebook-f"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-twitter"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-dribbble"></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 team-grid mt-sm-0 mt-5 text-center">
                    <img src="assets/images/manu.jpg" class="img-fluid" alt="">
                    <div class="caption">
                        <h4>Manuel Moncada</h4>
                        <ul class="list-unstyled w3pvt-icons">
                            <li>
                                <a href="#">
                                    <span class="fa fa-facebook-f"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-twitter"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-dribbble"></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 team-grid mt-lg-0 mt-5 text-center">
                    <img src="assets/images/willy.jpg" class="img-fluid" height="50px">
                    <div class="caption">
                        <h4>William Ochoa</h4>
                        <ul class="list-unstyled w3pvt-icons">
                            <li>
                                <a href="#">
                                    <span class="fa fa-facebook-f"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-twitter"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-dribbble"></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 team-grid  mt-lg-0 mt-5 text-center">
                    <img src="assets/images/chris.jpg" class="img-fluid" style="width: 700px; height: 260px;">
                    <div class="caption">
                        <h4>Christian Ruiz</h4>
                        <ul class="list-unstyled w3pvt-icons">
                            <li>
                                <a href="#">
                                    <span class="fa fa-facebook-f"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-twitter"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-dribbble"></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// gallery -->
    <!--/order-now-->
    <!--//order-now-->
    <!-- portfolio -->
    <!-- popup-->
    <!-- //popup -->
    <!--//portfolio-->

    <!--/testimonials-->
    <section class="testimonials" id="testimonials">
        <div class="layer test">
            <div class="container">
                <div class="test-info text-center">
                    <h3 class="my-md-2 my-3">Carlos Morales</h3>

                    <ul class="list-unstyled w3pvt-icons clients mb-md-4">
                        <li>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-star"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fa fa-star-half-o"></span>
                            </a>
                        </li>
                    </ul>
                    <p><span class="fa fa-quote-left"></span> El servicio que brinda Taxucontroll es totalmente de calidad. El control económico de mi empresa de taxis ha mejorado
                        de una forma increíble y todo gracias al servicio que nos brida Taxucontroll.<span class="fa fa-quote-right"></span></p>

                    </div>
                </div>
            </div>
        </section>
        <!--//testimonials-->

        <!-- contact -->
        <section class="contact py-5" id="contact">
            <div class="container pb-md-5">
                <div class="header py-lg-5 pb-3 text-center">
                    <h3 class="tittle mb-lg-5 mb-3">Resolvemos tus dudas!</h3>
                </div>

                <div class="contact-form mx-auto text-left">
                    <form name="contactform" id="contactform1" method="post" action="#">
                        <div class="row">
                            <div class="col-lg-4 con-gd">
                                <div class="form-group">
                                    <label>Nombre *</label>
                                    <input type="text" class="form-control" id="name" placeholder="" name="name" required="">
                                </div>
                            </div>
                            <div class="col-lg-4 con-gd">
                                <div class="form-group">
                                    <label>Correo *</label>
                                    <input type="email" class="form-control" id="email" placeholder="" name="email" required="">
                                </div>
                            </div>
                            <div class="col-lg-4 contact-page">
                                <div class="form-group">
                                    <label>*</label>
                                    <button type="submit" class="btn btn-default">Enviar</button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

        </section>
        <!-- //contact -->
        <!-- footer -->
        <footer>
            <div class="asap-lavi-footer-sec py-5">
                <div class="container py-lg-3">
                    <div class="row">
                        <div class="col-lg-5 asap-w3pvt-footer-logo">
                            <!-- footer logo -->
                            <a class="navbar-brand" href="index.html">Taxucontroll
                            </a>
                            <!-- //footer logo -->
                        </div>
                        <!-- button -->
                        <div class="col-lg-5 asap-w3pvt-footer text-lg-right text-center mt-3">
                            <ul class="list-unstyled footer-nav-lavi">
                                <li>
                                    <a href="index.php" class="scroll active">Inicio</a>
                                </li>
                                <li>
                                    <a class="scroll" href="#about">Nosotros</a>
                                </li>
                            </ul>
                        </div>
                        <!-- //button -->
                        <div class="col-lg-2 mt-lg-0 mt-3">
                            <div class="button-asap-w3pvts mt-0 text-lg-right text-center">
                                <a href="#contact" class="btn btn-sm thar-four scroll">Contáctenos</a>
                            </div>
                        </div>
                    </div>
                    <div class="row copy-right-sec mt-4 pt-lg-4 pt-3 text-lg-left text-center">
                        <!-- copyright -->
                        <p class="col-lg-8 copy-right-grids mt-lg-1">© 2020 Todos los derechos reservados
                            <a href="http://w3layouts.com/"> Taxucontroll </a></p>
                            <!-- //copyright -->
                            <!-- social icons -->

                            <!-- //social icons -->
                        </div>
                        <div class="move-top text-right"><a href="#home" class="move-top"> <span class="fa fa-angle-up  mb-3" aria-hidden="true"></span></a></div>
                    </div>
                </div>
            </footer>
            <!-- //footer -->
            <script src="assets/js/validacion.js"></script>
            <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        </body>

        </html>

