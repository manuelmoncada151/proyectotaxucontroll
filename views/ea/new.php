<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Entrega Ahorro</h2>
			</div>


			<div class="card-body">
				<form action="?controller=ea&method=save" method="post">
					<div class="form-group">
						<label>Valor</label>
						<input type="number" name="VALOR_ENTREAHORRO" class="form-control" placeholder="Valor">
					</div>
					<div class="form-group">
						<label>Fecha inicio</label>
						<input type="date" name="FECHAINICIO_CONVENIO" class="form-control" >
					</div>
					<div class="form-group">
						<label>Fecha fin</label>
						<input type="date" name="FECHAFIN_CONVENIO" class="form-control" >
					</div>
					<div class="form-group">
						<label>Convenio</label>
						<select name="ID_CONVENIO_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($convenios as $convenio) {
								 
										?>
											<option value="<?php echo $convenio->ID_CONVENIO ?>"><?php echo $convenio->MEDIDA_CONVENIO ?></option>
										<?php
									} 
								
							?>
						</select>
					</div>
				

					<div class="form-group">
						<button class="btn btn-primary">Guardar</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</main>