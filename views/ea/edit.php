<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Editar E.Ahorro</h2>
			</div>

			<div class="card-body">
				<form action="?controller=ea&method=update" method="post">
					<input type="hidden" name="ID_ENTREAHORRO" value="<?php echo $data[0]->ID_ENTREAHORRO ?>" >
				
					<div class="form-group">
						<label>Valor</label>
						<input type="number" name="VALOR_ENTREAHORRO" class="form-control" placeholder="Ingrese valor" value="<?php echo $data[0]->VALOR_ENTREAHORRO ?>">
					</div>
					<div class="form-group">
						<label>F. Inicio</label>
						<input type="date" name="FECHAINICIO_CONVENIO" class="form-control"  value="<?php echo $data[0]->FECHAINICIO_CONVENIO ?>">
					</div>
						<div class="form-group">
						<label>F. Fin</label>
						<input type="date" name="FECHAFIN_CONVENIO" class="form-control"  value="<?php echo $data[0]->FECHAFIN_CONVENIO ?>">
					</div>
				
					<div class="form-group">
						<label>Convenio</label>
						<select name="ID_CONVENIO_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($convenios as $convenio) {
									if($convenio->ID_CONVENIO == $data[0]->ID_CONVENIO_FK) {
										?>
											<option selected value="<?php echo $convenio->ID_CONVENIO ?>"><?php echo $convenio->MEDIDA_CONVENIO ?></option>
										<?php
									} else {
										?>
											<option value="<?php echo $convenio->ID_CONVENIO ?>"><?php echo $convenio->MEDIDA_CONVENIO ?></option>
										<?php
									}
								}
							?>
						</select>
					</div>

					<div class="form-group">
						<button class="btn btn-primary">Actualizar</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</main>