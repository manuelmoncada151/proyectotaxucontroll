<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Editar Clasificación</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Información de la Clasificación</header>
						<button id="panel-button3"
						class="mdl-button mdl-js-button mdl-button--icon pull-right"
						data-upgraded=",MaterialButton">
						<i class="fa fa-ellipsis-v"></i>
					</button>
					<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"data-mdl-for="panel-button3">
						<li>
							<a href="javascript:;"><i class="fa fa-print"></i> Print </a>
						</li>
						<li>
							<a href="javascript:;"><i class="fa fa-file-pdf-o"></i> Save as PDF </a>
						</li>
						<li>
							<a href="javascript:;"><i class="fa fa-file-excel-o"></i> Export to Excel </a>
						</li>
					</ul>
				</div>
				<div class="card-body " id="bar-parent2">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<form action="?controller=classif&method=update" method="post">
								<input type="hidden" name="ID_CLASINOV" value="<?php echo $data[0]->ID_CLASINOV?>" >
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Nombre</label>
								<input value="<?php echo $data[0]->NOM_CLASINOV?>" type="text" name="NOM_CLASINOV" class="mdl-textfield__input">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
								<label class="mdl-textfield__label"><label id="oblig">* </label> Descripción</label>
								<textarea style="min-height: 113px;" name="DESCRIP_CLASINOV"class="mdl-textfield__input"  required rows="3"
								placeholder="Enter ..."><?php echo $data[0]->DESCRIP_CLASINOV?></textarea>
							</div>
								<div class="form-group">
									<button class="btn btn-primary" id="submit">Actualizar</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>