<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Nuevo Vehiculo</li>
				</ol>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-box">
					<div class="card-head">
						<header>Nuevo Vehiculo</header>
						<button id="panel-button3"
						class="mdl-button mdl-js-button mdl-button--icon pull-right"
						data-upgraded=",MaterialButton">
						<i class="fa fa-ellipsis-v"></i>
					</button>
					<ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"data-mdl-for="panel-button3">
						<li>
							<a href="javascript:;"><i class="fa fa-print"></i> Print </a>
						</li>
						<li>
							<a href="javascript:;"><i class="fa fa-file-pdf-o"></i> Save as PDF </a>
						</li>
						<li>
							<a href="javascript:;"><i class="fa fa-file-excel-o"></i> Export to Excel </a>
						</li>
					</ul>
				</div>
			<div class="card-body">
				<!--<form action="?controller=veh&method=save" method="post" id="form">-->
					<!--<FORM enctype="multipart/form-data"></FORM>-->
					<div class="form-group">
						<label>Placa</label>
						<input type="text"  id="PLACA_VEHICULO" class="form-control" placeholder="Ingrese placa">
					</div>
					<!--	<div class="form-group">
						<label>Placa</label>
						<input type="file" name="REF_VEHICULO" class="form-control">
					</div>-->
					<div class="form-group">
						<label>Modelo</label>
						<input type="text"  id="MODELO_VEHICLO" class="form-control" placeholder="Ingrese modelo">
					</div>
					<div class="form-group">
						<label>Fecha</label>
						<input type="date" id="FECHAING_VEHICULO" class="form-control" >
					</div>
					<div class="form-group">
						<label>Marca</label>
						<select  id="ID_MARCA_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($brands as $brand) {
								 
										?>
											<option value="<?php echo $brand->ID_MARCA ?>"><?php echo $brand->NOM_MARCA ?></option>
										<?php
									} 
								
							?>
						</select>
					</div>
					<div class="form-group">
						<label>persona</label>
						<select id="ID_PERSONA_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($persons as $person) {
								 
										?>
											<option value="<?php echo $person->ID_PERSONA ?>"><?php echo $person->NOM_PERSONA ?></option>
										<?php
									} 
								
							?>
						</select>
					</div>


					<div class="form-group">
						<in type="submit" id="submit" class="btn btn-primary">Guardar</button>
					</div>
				<!--</form>-->

				<!--<?php

					if(isset($_POST['REF_VEHICULO'])) {

						$ruta = "assets/uploads/";
						$fichero = $ruta.basename($_FILES['REF_VEHICULO']['name']);
						if (move_uploaded_file($_FILES['REF_VEHICULO']['tmp_name'], $ruta.$_GET['ID_VEHICULO'])) {
						echo "subio";
						}
					}

				?>	
		-->

			</div>
		</div>
	</section>
</main>

<script src="assets/js/vehicles.js"></script>