<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Información del vehiculo</h2>
			</div>
			<div class="card-body">
				<form action="?controller=veh&method=update" method="post">
					<input type="hidden" name="ID_VEHICULO" value="<?php echo $data[0]->ID_VEHICULO ?>" >
					<input type="hidden" name="PLACA_VEHICULO" value="<?php echo $data[0]->PLACA_VEHICULO ?>" >
					<div class="form-group">
						<label>Modelo</label>
						<input type="number" name="MODELO_VEHICLO" class="form-control" placeholder="Ingrese Modelo" value="<?php echo $data[0]->MODELO_VEHICLO ?>">
					</div>
					<div class="form-group">
						<label>Fecha</label>
						<input type="date" name="FECHAING_VEHICULO" class="form-control"  value="<?php echo $data[0]->FECHAING_VEHICULO ?>">
					</div>
				
				
					<div class="form-group">
						<label>Marca</label>
						<select name="ID_MARCA_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($brands as $brand) {
									if($brand->ID_MARCA == $data[0]->ID_MARCA_FK) {
										?>
											<option selected value="<?php echo $brand->ID_MARCA ?>"><?php echo $brand->NOM_MARCA ?></option>
										<?php
									} else {
										?>
											<option value="<?php echo $brand->ID_MARCA ?>"><?php echo $brand->NOM_MARCA ?></option>
										<?php
									}
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Persona</label>
						<select name="ID_PERSONA_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($persons as $person) {
									if($person->ID_PERSONA == $data[0]->ID_PERSONA_FK) {
										?>
											<option selected value="<?php echo $person->ID_PERSONA ?>"><?php echo $person->NOM_PERSONA ?></option>
										<?php
									} else {
										?>
											<option value="<?php echo $person->ID_PERSONA ?>"><?php echo $person->NOM_PERSONA ?></option>
										<?php
									}
								}
							?>
						</select>
					</div>

					<div class="form-group">
						<button class="btn btn-primary">Actualizar</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</main>