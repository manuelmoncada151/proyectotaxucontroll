<?php
require('../../assets/fpdf/fpdf.php');

class PDF extends FPDF
{
// Cabecera de página
function Header()
{
    // Logo
    
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Movernos a la derecha
    $this->Cell(60);
    // Título
    $this->Cell(150,10,'Reporte de Novedades',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    $this->Cell(10,10,'Id',1,0,'C',0);
    $this->Cell(85,10,utf8_decode('Descripción'),1,0,'C',0);
    $this->Cell(25,10,'Fecha',1,0,'C',0);
    $this->Cell(25,10,'Valor',1,0,'C',0);
    $this->Cell(35,10,'Conductor',1,0,'C',0);
    $this->Cell(35,10,utf8_decode('Clasificación'),1,0,'C',0);
    $this->Cell(35,10,'Estado',1,1,'C',0);
}

// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}
}

require '../conexion2.php';

$consulta = "SELECT n.*,s.NOM_ESTADO as status, CONCAT(p.NOM_PERSONA,' ',p.APE_PERSONA) as person, c.NOM_CLASINOV as classif, v.ID_VEHICULO as vehiculo,v.PLACA_VEHICULO as vehi, v.ID_ESTADO_FK as estveh
                            FROM novedad n 
                            INNER JOIN estado s ON s.ID_ESTADO = n.ID_ESTADO_FK 
                            INNER JOIN persona p ON p.ID_PERSONA = n.ID_PERSONA_FK 
                            INNER JOIN convenio_producido co ON co.ID_PERSONA_FK = p.ID_PERSONA
                            INNER JOIN vehiculo v ON v.PLACA_VEHICULO = co.PLACA_VEHICULO_FK
                            INNER JOIN estado se ON se.ID_ESTADO = v.ID_ESTADO_FK 
                            INNER JOIN clasificacion_novedad c ON c.ID_CLASINOV=n.ID_CLASINOV_FK 
                            INNER JOIN usuario U ON U.ID_USUARIO=p.ID_USUARIO_FK order by n.ID_NOVEDAD";
$resultado = $mysqli->query($consulta);

$pdf = new PDF();

// $pdf->set_paper('letter','landscape');
$pdf -> AliasNbPages();
$pdf->AddPage('LANDSCAPE','letter');
$pdf->SetFont('Arial','B',8);

while ($row = $resultado->fetch_assoc()) {
    $pdf->Cell(10,10,$row['ID_NOVEDAD'],1,0,'C',0);
    $pdf->Cell(85,10,utf8_decode($row['DESCRIP_NOVEDAD']),1,0,'C',0);
    $pdf->Cell(25,10,$row['FECH_NOVEDAD'],1,0,'C',0);
    $pdf->Cell(25,10,$row['VALOR_NOVEDAD'],1,0,'C',0);
    $pdf->Cell(35,10,$row['person'],1,0,'C',0);
    $pdf->Cell(35,10,$row['classif'],1,0,'C',0);
    $pdf->Cell(35,10,$row['status'],1,1,'C',0);
}

$pdf->Output();
?>