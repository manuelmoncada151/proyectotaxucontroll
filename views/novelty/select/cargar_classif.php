<?php 
require_once 'conexion.php';

function getListasRep(){
  $mysqli = getConn();
  $query = 'SELECT * FROM `CLASIFICACION_NOVEDAD`';
  $result = $mysqli->query($query);
  $listas = '<option value=""></option>';
  while($row = $result->fetch_array(MYSQLI_ASSOC)){
    $listas .= "<option value='$row[ID_CLASINOV]'>$row[NOM_CLASINOV]</option>";
  }
  return $listas;
}

echo getListasRep(); 