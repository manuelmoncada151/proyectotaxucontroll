<!--Apertura container-->

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Novedades</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-topline-red">
					<div class="card-head">
						<header>Listado de Novedades</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							
						</div>
					</div>
					<div class="card-body ">
						<div class="row p-b-20">
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group">
									<?php 
									$estadoveh=$noveltys[0]->estveh;
									if( $_SESSION['user']->ID_ROL_FK ==3){
										if($estadoveh==1){?>
											<a href="?controller=novelty&method=add" id="addRow1" class="btn btn-info">
												Nuevo <i class="fa fa-plus"></i>
											</a>
										<?php	}if($estadoveh==2){ ?>
											<span class="label label-sm box-shadow-1 label-danger">tu vehiculo está inactivo</span>
										<?php	}		?>
										
										<?php 
									}
									?>
								</div>
							</div>
							
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group pull-right">
									<button class="btn deepPink-bgcolor  btn-outline dropdown-toggle"
									data-toggle="dropdown">Opciones
									<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<!-- <li>
										<a href="javascript:;">
											<i class="fa fa-print"></i> Imprimir </a>
										</li> -->
										<li>
											<a href="views/novelty/pdfnovelty.php">
												<i class="fa fa-file-pdf-o"></i> Exportar a PDF </a>
											</li>
											<!-- <li>
												<a href="javascript:;">
													<i class="fa fa-file-excel-o"></i> Exportar a Excel </a>
												</li> -->
											</ul>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<div class="table-responsive tblDriverDetail">
										<table class="table display product-overview mb-30" id="dt1">
											<thead>
												<tr>
													<th>#</th>
													<th>Descripción</th>
													<th>Fecha</th>
													<th>Soporte</th>
													<th>Valor</th>
													<th>Persona</th>
													<th>Vehículo</th>
													<th>Clasificaciones</th>
													<th>Estado</th>
													<th>Tipos</th>
													<?php
													if($_SESSION['user']->ID_ROL_FK ==1){
														?>
														<th>Acciones</th>
														<?php
													}
													?>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($noveltys as $novelty) : ?>
													<tr>
														<td><?php echo $novelty->ID_NOVEDAD ?></td>
														<td><?php echo $novelty->DESCRIP_NOVEDAD ?></td>
														<td><?php echo $novelty->FECH_NOVEDAD ?></td>
														<td><?php echo $novelty->URL_SUPNOV ?></td>
														<td><?php echo $novelty->VALOR_NOVEDAD ?></td>
														<td><?php echo $novelty->person ?></td>
														<td><?php echo $novelty->vehi ?> 
														<?php 
														if( $_SESSION['user']->ID_ROL_FK ==3){
															if ($novelty->estveh == 1){
																?>


																<?php
															}else{
																?>
																<a id="activar" type="button" onclick="validar()" class="btn btn-tbl-activar btn-xs" href="?controller=novelty&method=updateStatusVeh&id=<?php echo $novelty->vehiculo ?>"><i class="fa fa-unlock "></i></a>
																<?php
															}
														}
														?>
													</td>
													<td><?php echo $novelty->classif ?></td>
													<td>
														<?php 
														if ($novelty->ID_ESTADO_FK == 3){
															?>
															<span class="label label-sm box-shadow-1 label-danger"><?php echo $novelty->status ?></span>
															<?php
														}else{
															?>
															<span class="label label-sm box-shadow-1 label-success"><?php echo $novelty->status ?></span>
															<?php
														}
														?>
														<td>
															<a class="btn btn-tbl-delete btn-xs" href="?controller=DetNovelty&method=listar&id=<?php echo $novelty->ID_NOVEDAD ?>"><i class="fa fa-eye "></i></a>



														</td>
														<?php 
														if($_SESSION['user']->ID_ROL_FK ==1){
															?>
															<td>
																<?php 
																if ($novelty->ID_ESTADO_FK == 3){
																	?>
																	<!-- <a class="btn btn-tbl-edit btn-xs" href="?controller=novelty&method=updateStatus&id=<?php echo $novelty->ID_NOVEDAD ?>"><i class="fa fa-check"></i></a> -->
																	<button class="btn btn-tbl-edit btn-xs" data-href="?controller=novelty&method=updateStatus&id= <?php echo $novelty->ID_NOVEDAD ?>" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-check"></i></button>
																	<?php
																}
															}
															?>

														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						if( $_SESSION['user']->ID_ROL_FK ==1){
					?>
					<div class="col-sm-6">
					<div class="card card-topline-red">
						<div class="card-head">
							<header>Gastos por conductor</header>
							<div class="tools">
								<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
								<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
								<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
							</div>
						</div>
						<div class="card-body">
							<button class="btn btn-primary" onclick="CargarDatos()">Cargar</button>
							<canvas id="myChart" ></canvas>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card card-topline-red">
						<div class="card-head">
							<header>Novedades por conductor</header>
							<div class="tools">
								<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
								<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
								<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
							</div>
						</div>
						<div class="card-body">
							<button class="btn btn-primary" onclick="CargarDatos2()">Cargar</button>
							<canvas id="myChart2" ></canvas>
						</div>
					</div>
				</div>
				<?php
				}
				?>
				</div>
				
			</div>
		</div>

		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">

						<h3 class="modal-title" id="exampleModalLongTitle"><strong>¡Espera! ¿Estas seguro de confirmar esta novedad?</strong></h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>Si tu confirmas esta novedad, ya no habrá vuelta atras...</p>
						<p><small>Debes confirmar las novedades solo cuando ya se haya encontrado una solución</small></p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-success btn-ok">Confirmar</a>
					</div>
				</div>
			</div>
		</div>
		<script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});

		</script>
		<div class="modal fade" id="confirm-activar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">

						<h3 class="modal-title" id="exampleModalLongTitle"><strong>¡Espera!</strong></h3>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p>¿Estas seguro de activar el vehículo?</p>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-success btn-ok">Confirmar</a>
					</div>
				</div>
			</div>
		</div>
		<script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});

		</script>
		<script src="assets/js/validacion.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

		<script>

			function CargarDatos(){
					$.ajax({
						url: 'views/novelty/GraficoController.php',
						type: 'POST'
					}).done(function(resp){
						var titulo = [];
						var cantidad = [];
						var data = JSON.parse(resp);
						for(var i=0;i<data.length;i++){
							titulo.push(data[i][8]);
							cantidad.push(data[i][9]);
						}
						var ctx = document.getElementById('myChart');
						var myChart = new Chart(ctx, {
							type: 'bar',
							data: {
								labels: titulo,
								datasets: [{
									label: 'Limpiar' ,
									data: cantidad,
									backgroundColor: [
									'#FAF674',
									'#B3B3AB',
									'#FAF674',
									'#B3B3AB',
									'#FAF674',
									'#B3B3AB',
									'#FAF674'
									],
								}]
							},
							options: {
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero: true
										}
									}]
								}
							}
						});
					})
				}

			function CargarDatos2(){
				$.ajax({
					url: 'views/novelty/GraficoController.php',
					type: 'POST'
				}).done(function(resp){
					var titulo = [];
					var cantidad = [];
					var data = JSON.parse(resp);
					for(var i=0;i<data.length;i++){
						titulo.push(data[i][8]);
						cantidad.push(data[i][10]);
					}
					var ctx = document.getElementById('myChart2');
					var myPieChart = new Chart(ctx, {
						type: 'pie',
						data: data = {
							datasets: [{
								data:  cantidad,
								backgroundColor: [
								'#FAF674',
								'#B3B3AB',
								'#F6AAA3 ',
								'#C6F6A3 ',
								'#A3BEF6 '
								],
								borderWidth: 1
							}],
    							// These labels appear in the legend and in the tooltips when hovering different arcs
    							labels: titulo,
    						},
    						options: cantidad
    					});
				})
			}
		</script>
		