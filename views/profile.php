  <link rel="stylesheet" href="assets/css/sweetalert.min.css">
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title">Dashboard Taxucontroll
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
            href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Perfil</li>
        </ol>
      </div>
    </div>
    <div class="row">
            <div class="col-md-12">
              <!-- BEGIN PROFILE SIDEBAR -->
              <div class="profile-sidebar">
                <div class="card card-topline-aqua">
                  <div class="card-body no-padding height-9">
                    <div class="row">
                      <div style="margin-left: 140px">
  
                       <?php if ($_SESSION['user']->ID_ROL_FK==3) {?>
                        <img  src="assets/icons/marcador-de-mapa.png"  >
                      <?php } ?>
                      <?php if ($_SESSION['user']->ID_ROL_FK==2) {?>
                        <img  src="assets/icons/usuario.png" >
                      <?php } ?>
                      <?php if ($_SESSION['user']->ID_ROL_FK==1) {?>
                        <img  src="assets/images/ingeniero.png" alt="" >
                      <?php } ?>
                      
                    </div>
                  </div>
                    <div class="profile-usertitle">
                      <div class="profile-usertitle-name"> <?php echo $person[0]->NOM_PERSONA?> <?php echo $person[0]->APE_PERSONA?></div>
                      <div class="profile-usertitle-job"><?php echo $_SESSION['user']->role ?></div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                    <!-- END SIDEBAR BUTTONS -->
                  </div>
                </div>
                <div class="card">
                  <div class="card-head card-topline-aqua">
                    <header>IMPORTANTE</header>
                  </div>
                  <div class="card-body no-padding height-9">
                    <div class="profile-desc">
                      La información que ves son datos de nuestra base de datos (Solo tú y el administrador tienen acceso a ellos), si alguno de estos datos está mal o incompleto y no lo puedes editar por favor contactate con el administrador, la integridad de los mismos son muy importantes. 
                    </div>
                  </div>
                </div>
                
              </div>
              <!-- END BEGIN PROFILE SIDEBAR -->
              <!-- BEGIN PROFILE CONTENT -->
              <div class="profile-content">
                <div class="row">
                  <div class="profile-tab-box">
                    <div class="p-l-20">
                      <ul class="nav ">
                        <li class="nav-item tab-all"><a class="nav-link active show"
                            href="#tab1" data-toggle="tab">Perfil</a></li>
                        <li class="nav-item tab-all p-l-20"><a class="nav-link" href="#tab3"
                            data-toggle="tab">Cambiar Contraseña</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="white-box">
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane active fontawesome-demo" id="tab1">
                        <div id="biography">
                          <div class="row">
                            <div class="col-md-3 col-6 b-r"> <strong>Nombre completo</strong>
                              <br>
                              <p class="text-muted"><?php echo $person[0]->NOM_PERSONA?> <?php echo $person[0]->APE_PERSONA?></p>
                            </div>
                            <div class="col-md-3 col-6 b-r"> <strong>Telefono celular</strong>
                              <br>
                              <p class="text-muted"><?php echo $person[0]->TEL_PERSONA?></p>
                            </div>
                            <div class="col-md-3 col-6 b-r"> <strong>Email</strong>
                              <br>
                              <p class="text-muted"><?php echo $person[0]->CORREO_PERSONA?></p>
                            </div>

                          </div>
                          <hr>
                          <div class="profile-usertitle-name"><h2>Editar Información</h2></div>
                          <form method="POST" action="?controller=user&method=updateuser">
                          <p class="m-t-30">
                           <div class="row">
                            <div class="col-md-4 col-2 b-r"> 
                              <label>Nombre</label>
                              <input type="text" value="<?php echo $person[0]->NOM_PERSONA?>" class="form-control" name="NOM_PERSONA">
                              <label>Celular</label>
                              <input type="number" value="<?php echo $person[0]->TEL_PERSONA?>" class="form-control" name="TEL_PERSONA">
                              <small>Taxucontroll está contigo, reporta un problema</small>
                            </div>
                            <div class="col-md-4 col-6 b-r"> 
                              <label>Apellido</label>
                              <input type="text" value="<?php echo $person[0]->APE_PERSONA?>" class="form-control" name="APE_PERSONA">
                              <label>Email</label>
                              <input type="email" value="<?php echo $user[0]->NOM_USUARIO?>" class="form-control" name="NOM_USUARIO">
                            </div>
                            <div class="col-md-2 col-6 b-r">
                            </div>
                            <div class="col-md-2 col-6"> 
                              <br>
                              <p><button class="btn btn-warning">Actualizar</button></p>
                            </div>
                          </div>
                          <br>
                          </form>
                        </div>
                      </div>
                      <div class="tab-pane" id="tab3">
                        <div class="row">
                          <div class="col-md-12 col-sm-12">
                            <div class="card-head">
                              <header>Cambiar Contraseña</header>

                            </div>
                            <div class="card-body " id="bar-parent1">
                              <form action="?controller=user&method=changepass" method="POST">
                                <div class="form-group">
                                  <label for="simpleFormPassword">Contraseña antigua</label>
                                  <input type="password" name="lastpass" class="form-control"
                                    id="simpleFormPassword"
                                    placeholder="Escriba su ultima contraseña">
                                </div>
                                <div class="form-group">
                                  <label for="simpleFormPassword">Nueva Contraseña</label>
                                  <input type="password" name="newpass" class="form-control"
                                    id="newpassword" placeholder="Escriba su nueva contraseña">
                                </div>
                                <button type="submit"
                                  class="btn btn-primary">Cambiar</button>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<script src="assets/js/sweetalert.min.js"></script>
  <script src="assets/js/sweet-alert-data.js"></script>



































   