<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Información del usuario</h2>
			</div>
			<div class="card-body">
				<form action="?controller=User&method=save" method="post">
					<div class="form-group">
						<label>Documento</label>
						<input type="number" name="DOCU_PERSONA" class="form-control" placeholder="Ingrese documento"required>
					</div>
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="NOM_PERSONA" class="form-control" placeholder="Ingrese nombre" required>
					</div >
					<div class="form-group">
						<label>Apellido</label>
						<input type="text" name="APE_PERSONA" class="form-control" placeholder="Ingrese apellido" required>
					</div>

					<div class="form-group">
						<label>Rol</label>
						<select name="ID_ROL_FK" class="form-control">
							<option value="" required>Seleccione...</option>											
							<?php 
								foreach($rols as $rol) {
								 
										?>
											<option value="<?php echo $rol->ID_ROL ?>"><?php echo $rol->NOM_ROL ?></option>
										<?php
									} 
								
							?>
						</select>
					</div>
					<div class="form-group">
						<label>Teléfono</label>
						<input type="number" name="TEL_PERSONA" class="form-control" placeholder="Ingrese telefono" required>
						
					</div>
					<div class="form-group">
						<label>Correo</label>
						<input type="email" name="CORREO_PERSONA" class="form-control" placeholder="Ingrese correo" required>
					</div>
					<div class="form-group">
						<label>Dirección</label>
						<input type="text" name="DIREC_PERSONA" class="form-control" placeholder="Ingrese dirección" required>
					</div>
					<div class="form-group">
						<label>Nombre usuario</label>
						<input type="text" name="NOM_USUARIO" class="form-control" placeholder="Ingrese usuario"required>
					</div>
					<div class="form-group">
						<label>Contraseña</label>
						<input type="password" name="PASSWORD" class="form-control" placeholder="Ingrese contraseña"required>
					</div>
						<div class="form-group">
						<label>Confirmar contraseña</label>
						<input type="password" name="PASSWORD" class="form-control" placeholder="confirme contraseña"required>
					</div>
					<div class="form-group">
						<button class="btn btn-primary">Guardar</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</main>