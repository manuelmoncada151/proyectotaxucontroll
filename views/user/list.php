<!--Apertura container-->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Usuarios</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-topline-red">
					<div class="card-head">
						<header>Listado de Usuarios</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
						</div>
					</div>
					<div class="card-body ">
						<div class="row p-b-20">
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group">
									<a href="?controller=User&method=add" id="addRow1" class="btn btn-info">
										Nuevo <i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group pull-right">
									<button class="btn deepPink-bgcolor  btn-outline dropdown-toggle"
									data-toggle="dropdown">OPCIONES
									<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;">
											<i class="fa fa-print"></i>Imprimir</a>
										</li>
										<li>
											<a href="javascript:;">
												<i class="fa fa-file-pdf-o"></i> Exportar a PDF </a>
											</li>
											<li>
												<a href="javascript:;">
													<i class="fa fa-file-excel-o"></i> Exportar a Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<div class="table-responsive tblDriverDetail">
										<table class="table table-striped table-hover" id="example" style="margin-top:50px;">
											<thead>
												<tr>
													<th>Documento</th>
													<th>Nombre</th>
													<th>Rol</th>
													<th>Teléfono</th>
													<th>Correo</th>
													<th>Dirección</th>
													<th>Usuario</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($lstUsers as $user) : ?>
													<tr>
														<td><?php echo $user->DOCU_PERSONA ?></td>
														<td><?php echo $user->NOM_PERSONA.' '.$user->APE_PERSONA ?></td>
														<td><?php echo $user->NOM_ROL ?></td>
														<td><?php echo $user->TEL_PERSONA ?></td>
														<td><?php echo $user->CORREO_PERSONA ?></td>
														<td><?php echo $user->DIREC_PERSONA ?></td>
														<td><?php echo $user->NOM_USUARIO ?></td>
														<td>
															<?php
															if($user->ID_ESTADO_FK ==1)
															{
																?>
																<a href="?controller=User&method=updateStatus&id=<?php echo $user->ID_USUARIO ?>" class="label label-sm box-shadow-1 label-success">Activo</a>
																<a href="?controller=User&method=edit&id=<?php echo $user->ID_USUARIO ?>" class="btn btn-tbl-delete btn-xs"><i class="fa fa-edit "></i></a>
																<a href="?controller=User&method=delete&id=<?php echo $user->ID_USUARIO?>" class="btn btn-tbl-eliminar btn-xs"><i class="fa fa-trash-o "></i>
																	<?php
																} else {
																	?>
																	<a href="?controller=User&method=updateStatus&id=<?php echo $user->ID_USUARIO?>" class="label label-sm box-shadow-1 label-danger">Inactivo</a>
																	<?php
																}
																?>
															</a>
														</td>
													</tr>
												<?php endforeach ?> 
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>