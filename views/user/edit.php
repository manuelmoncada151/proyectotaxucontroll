<script type="text/javascript">
	function editarConvenio(idConvenio){
		if(idConvenio!=null && idConvenio!=''){

		}else{
			alert('No hay convenios')
		}
		location.href='?controller=convenio&method=edit&id='+idConvenio;
	}
</script>
<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Información del usuario</h2>
			</div>
			<div class="card-body">
				<form action="?controller=User&method=update" method="post">
					<input type="hidden" name="ID_USUARIO" value="<?php echo $data[0]->ID_USUARIO ?>" >
					<input type="hidden" name="ID_USUARIO" value="<?php echo $data[0]->ID_USUARIO ?>" >

					<div class="form-group">
						<label>Documento</label>
						<input type="text" name="DOCU_PERSONA" class="form-control" placeholder="Ingrese documuento" value="<?php echo $data[0]->DOCU_PERSONA ?>" readonly="readonly">
					</div>
					
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="NOM_PERSONA" class="form-control" placeholder="Ingrese nombre" value="<?php echo $data[0]->NOM_PERSONA ?>">
					</div>

					<div class="form-group">
						<label>Apellido</label>
						<input type="text" name="APE_PERSONA" class="form-control" placeholder="Ingrese apellido" value="<?php echo $data[0]->APE_PERSONA ?>">
					</div>

					<div class="form-group">
						<label>Rol</label>
						<select name="ID_ROL_FK" class="form-control">
							<option value="">Seleccione...</option>											
							<?php 
								foreach($rols as $rol) {
									if($data[0]->ID_ROL_FK == $rol->ID_ROL)
									{
										?>
											<option selected="true" value="<?php echo $rol->ID_ROL ?>"><?php echo $rol->NOM_ROL ?></option>
										<?php

									}else{
										?>
											<option value="<?php echo $rol->ID_ROL ?>"><?php echo $rol->NOM_ROL ?></option>
										<?php
									}
								} 
								
							?>
						</select>
					</div>

					<div class="form-group">
						<label>Telefono</label>
						<input type="number" name="TEL_PERSONA" class="form-control" placeholder="Ingrese telefono" value="<?php echo $data[0]->TEL_PERSONA ?>">
					</div>

					<div class="form-group">
						<label>Correo</label>
						<input type="email" name="CORREO_PERSONA" class="form-control" placeholder="Ingrese correo" value="<?php echo $data[0]->CORREO_PERSONA ?>">
					</div>

					<div class="form-group">
						<label>Dirección</label>
						<input type="text" name="DIREC_PERSONA" class="form-control" placeholder="Ingrese direccion" value="<?php echo $data[0]->DIREC_PERSONA ?>">
					</div>

					<div class="form-group">
						<label>Nombre usuario</label>
						<input type="text" name="NOM_USUARIO" class="form-control" placeholder="Ingrese nombre usuario" value="<?php echo $data[0]->NOM_USUARIO ?>">
					</div>
					<?php //VALIDAMOS QUE EL ID DEL ROL SEA CODUCTOR (2) 
							if( $data[0]->ID_ROL_FK == "2" ){ 
					?>
					<br>
					<div class="form-group">
										<table align="center" border="0">
											<tr>
												<td align="center">
													Convenios a cargo de <?php echo $data[0]->NOM_PERSONA .' '. $data[0]->APE_PERSONA ?>.
													<br>
													<span style="font-size: 12px">(Doble click para editar convenio)</span>
												 </td>
											</tr>
											<tr>
												<td rowspan="4" align="center">

													<select class="form-control" style="width:310px" size="7" name="cmbconduasig" id="cmbconduasig" ondblclick="editarConvenio(this.value)" title='Doble click para editar convenio'>
														<?php 
															foreach($lstConductoresAsignados as $asig) {
																
																	?>
																		<option value="<?php echo $asig->ID_CONVENIO ?>">
																			<?php echo 
																			$asig->ID_CONVENIO .' - ' . 
																			$asig->NOM_COND ?>
																			</option>
																	<?php
															} 
															
														?>
													</select>  
												</td>
											</tr>
										</table>
									</div>
					<!--<div class="form-group">
										<table align="center" border="0">
											<tr>
												<td align="center">Conductores disponibles &nbsp;</td>
												<td>&nbsp;</td>
												<td align="center">Conductores asignados</td>
											</tr>
											<tr>
												<td rowspan="4" align="center">&nbsp;&nbsp;
													<select style="width:200px" size="15" name="cmbrecursosdisponibles" id="cmbrecursosdisponibles">
													</select>   
												</td>
												<td>&nbsp;</td>
												<td rowspan="4" align="center">&nbsp;&nbsp;&nbsp;
													<select style="width:200px" size="15" name="cmbrecursosgerente" id="cmbrecursosgerente">
													</select>  
												</td>
											</tr>

											<tr align="center" valign="top">
												<td class="texto11Azul"><input type="button" class="btn btn-success" name="enviar" value="Asignar >>" onClick="llenado(document.form1.cmbrecursosdisponibles,document.form1.cmbrecursosgerente,true)"/>
												</td>
											</tr>

											<tr align="center" valign="top">
												<td >&nbsp;</td>
											</tr>

											<tr align="center" valign="top">
												<td class="texto11Azul"><input type="button" class="btn btn-danger" name="quitar" value="<< Quitar" onClick="llenado(document.form1.cmbrecursosgerente,document.form1.cmbrecursosdisponibles,false)"/>
												</td>
											</tr>
										</table>
									</div>-->

					<?php } ?>

					<div class="form-group">
						<br>
						<button class="btn btn-primary">Actualizar</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</main>