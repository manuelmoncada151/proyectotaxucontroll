<!--Apertura container-->

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">Novedades</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-topline-red">
					<div class="card-head">
						<header>Tipos de la novedad</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							
						</div>
					</div>
					<div class="card-body ">
						<div class="row p-b-20">
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group">

									<a href="?controller=novelty" id="addRow1" class="btn btn-dark">
										<i class="fa fa-arrow-circle-left"></i> Volver 
									</a>
								</div>
								<!-- <form action="#" method="get" class="form_search_date">
									<label>De: </label>
									<input type="date" name="fecha_de" id="fecha_de" required>
									<label>A </label>
									<input type="date" name="fecha_a" id="fecha_a" required>
									<button type="submit" class="btn_view">buscar</button>
									
								</form> -->
							</div>

								</div>
								<div class="table-wrap">
									<div class="table-responsive tblDriverDetail">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Tipo</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($detnoveltys as $detnovelty) : ?>
										<tr>
											<td><?php echo $detnovelty->ID_NOVEDAD_FK ?></td>
											<td><?php echo $detnovelty->type ?></td>
										</tr>
									<?php endforeach ?> 
								</tbody>
							</table>
						</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


