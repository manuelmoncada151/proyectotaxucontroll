<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/login.css">
    <link rel="icon" href="assets/images/taxicon.png" type="image/ico" />
    <title>Login | Taxucontroll</title>
  </head>
  <body>
    <div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          <form id="formlg" action="?controller=login&method=login" method="post" class="sign-in-form">
            <h2 class="title">Iniciar Sesión</h2>
             <?php

            if(isset($error['errorMessage'])){

            ?>
            <div class="alert alert-danger alert-dismissable alert-width" role="alert">
              <p class="text-dark" style="color: red;"><?php echo $error['errorMessage'] ?></p>
            </div>
            <?php
            }
            ?> 
            <div class="input-field">
              <i class="fas fa-user"></i>
              <input type="text" placeholder="Usuario" name="nombre" value="<?php echo isset($error['nombre']) ? $error['nombre']: '' ?>"/>
            </div>
            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="password" placeholder="Contraseña"/>
            </div>
            <input type="submit" value="Ingresar" class="btn solid" />
            <div class="social-media">
            </div>
          </form>
          <form action="?controller=resetpassword&method=validateuser" method="POST" class="sign-up-form">
            <h2 class="title">Recuperar Cuenta</h2>
            <div class="input-field">
              <i class="fas fa-user"></i>
              <input type="text" name="name" required placeholder="Nombre" id="txt_nombre" />
            </div>
            <div class="input-field">
              <i class="fas fa-envelope"></i>
              <input type="email" required placeholder="Correo" name="correo" id="txt_correo" />
            </div>
            <!-- <input type="submit" class="btn" onclick="EnviarCorreo()" value="Enviar"/> -->
            <button style="text-align: center;" type="submit" class="btn btn-warning" value="Enviar">Enviar</button>
            <div class="social-media">
            </div>
          </form>

        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
            <img src="assets/images/tax8.png" class="logo-brand" alt="logo" style="height:50px ;margin-top: -20px;margin-bottom: 20px;">
            <h3>¿Olvidaste el usuario o la contraseña?</h3>
            <p>
              No te preocupes! Selecciona la opción de abajo.
            </p>
            <button class="btn transparent" id="sign-up-btn">
              Recuperar
            </button>

          </div>
          <img src="assets/images/register.svg" class="image" alt="" />
        </div>
        <div class="panel right-panel">
          <div class="content">
            <img src="assets/images/tax8.png" class="logo-brand" alt="logo" style="height:50px ;margin-top: -20px;margin-bottom: 20px;">
            <h3>Taxucontroll esta contigo!</h3>
            <p>
              Una vez digitados los datos requeridos
              revisa tu correo y sigue los pasos para iniciar sesión!
            </p>
            <button class="btn transparent" id="sign-in-btn">
              Iniciar Sesión
            </button>
          </div>
          <img src="assets/images/log.svg" class="image" alt="" />
        </div>
      </div>
    </div>


    <script src="assets/js/app.js"></script>
    
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="assets/js/validacion.js"></script>
  </body>
</html>

