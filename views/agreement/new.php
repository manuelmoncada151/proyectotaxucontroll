<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title">Dashboard Taxucontroll

        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
            href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Nuevo Convenio</li>
        </ol>
      </div>
    </div>

    <div class="row">
     <div class="col-md-12 col-sm-12">
       <div class="card  card-box">
        <div class="card-header ">
          <h2 class="m-auto">Agregar nuevo convenio</h2>
        </div>
        <div class="col-md-12" style="margin-left: 20px">
          <div class="well well-sm">
            <form class="form-horizontal" action="?controller=convenio&method=save" method="post">
              <fieldset>
               <div class="form-group">
                 <div class="row">
                  <div class="col-md-5 ">
                   <strong>Fecha inicio convenio</strong> 
                    <div class="row">
                     <img  src="assets/icons/calendario (1).png" height="30" width="30"> 
                     <div class="col">
                      <input class="form-control" type="date" name="FECHAINICIO_CONVENIO">
                    </input>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <strong> Fecha Fin convenio</strong>
                <div class="row">
                 <img  src="assets/icons/calendario (2).png"  height="30" width="30">
                 <div class="col">
                   <input class="form-control" type="date" name="FECHAFIN_CONVENIO">
                 </input>
               </div>

             </img>
           </div>
         </div>
       </div>
     </div>
      <strong> Valor producido diario</strong>
     <div class="form-group">
      <div class="row">
        <img src="assets/icons/dinero.png" height="30" width="30">
        <div class="col-md-5 ">
          <input type="number" class="form-control" required name="VALOR_CONVENIO" placeholder="Valor producido diario">
        </div>
        <img src="assets/icons/ahorro.png" height="30" width="30">
        <div class="col-md-5 ">
          <input type="number" class="form-control"  required name="AHORRO_PRODUCIDO"  placeholder="Valor ahorro diario">
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <img src="assets/icons/usuario.png" height="30" width="30">
        <div class="col-md-5 ">
          <select name="ID_PERSONA_FK"  required class="form-control">
            <option value="">Seleccione:</option>
            <?php
            foreach($persons as $person):{
              ?><option  value="<?php echo$person->ID_PERSONA ?>"><?php echo $person->NOM_PERSONA ?> <?php echo $person->APE_PERSONA ?></option>
              <?php
            }
          endforeach ?>
        </select>
      </div>
      <img src="assets/icons/numero.png" height="30" width="30">
      <div class="col-md-5 ">
       <select name="PLACA_VEHICULO_FK"  required class="form-control">
         <?php
         foreach($cars as $car){ ?>

          <option  value="<?php echo $car->PLACA_VEHICULO ?>"><?php echo $car->PLACA_VEHICULO ?></option>

        <?php } ?>
      </select>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <img src="assets/icons/regla.png" height="30" width="30">
    <div class="col-md-5 ">
      <select name="MEDIDA_CONVENIO" class="form-control">
        <option value="Diario">Medida...</option>
        <option value="Diario">Diario</option>
        <option value="Semanal">Semanal</option>
        <option value="Semanal">Quincenal</option>
        <option value="Mensual">Mensual</option>
      </select>
    </div>
    <img src="assets/icons/salvar.png" height="30" width="30">
    <div class="col-md-5 ">
     <button type="submit" class="btn btn-outline-warning btn-lg">Guardar</button>
   </div>
 </div>
</div>

</fieldset>
</form>
</div>
</div>
</div>
</section>
</section>

</main>
</div>

</div>
</div>
