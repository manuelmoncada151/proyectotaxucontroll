<div class="page-content-wrapper">
        <div class="page-content">
          <div class="page-bar">
            <div class="page-title">Dashboard Taxucontroll
        
              <ol class="breadcrumb page-breadcrumb pull-right">
                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                    href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
                </li>
                <li class="active">Pagos</li>
              </ol>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                  <div class="card">
                    <div class="panel-body">
                      <h3>El promedio del valor convenio es:</h3>
                      <div class="progressbar-xs progress box-shadow-1">
                        <div class="progress-bar progress-bar-green width-100" role="progressbar"
                          aria-valuenow="65" aria-valuemin="0" aria-valuemax="50"></div>
                      </div>
                      <span class="text-small margin-top-10 full-width"><?php echo $promedio[0]->promedio ?></span>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                  <div class="card">
                    <div class="panel-body">
                      <h3>Cantidad de coductores</h3>
                      <div class="progressbar-xs progress box-shadow-1">
                        <div class="progress-bar progress-bar-orange width-100"
                          role="progressbar" aria-valuenow="100" aria-valuemin="0"
                          aria-valuemax="100"></div>
                      </div>
                      <span class="text-small margin-top-10 full-width">En nuestro sistema tienes: <?php echo $conductores[0]->NUM ?> conductores registrados</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
           <div class="col-md-12 col-sm-12">
             <div class="card  card-box">
            <h1 style="margin-top: 50px; margin-left: 10px"> Listado de Convenio</h1>
             <label></label>
            <section class="col-md-12 table-responsive">
              <table class="table table-striped table-hover" id="example" style="margin-top:50px;">
                 <a href="?controller=convenio&method=add" class="btn btn-success">Nuevo</a>
                <thead>
                  <tr>
                   <tr>
                    <th scope="col">#</th>
                    <th scope="col">Fecha Inicio</th>
                    <th scope="col">Fecha Fin</th>
                    <th scope="col">Medida</th>
                    <th scope="col">Valor Diario Convenio</th>
                    <th scope="col">Valor Diario Ahorro</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Vehículos</th>
                    <th scope="col">Conductores</th>
                    <th scope="col">Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($convenios as $convenio)  : ?>
                    <tr>
                      <td><?php echo $convenio->ID_CONVENIO ?></td>
                      <td><?php echo $convenio->FECHAINICIO_CONVENIO ?></td>
                      <td><?php echo $convenio->FECHAFIN_CONVENIO ?></td>
                      <td><?php echo $convenio->MEDIDA_CONVENIO ?></td>
                      <td><?php echo $convenio->VALOR_CONVENIO ?></td>
                      <td><?php echo $convenio->AHORRO_PRODUCIDO ?></td>
                      <?php if($convenio->S_N_CANCELADO ==2){?>
                        
                           <td>Sin cancelar</td>
                      <?php } ?>
                      <?php if($convenio->S_N_CANCELADO ==1){?>
                           <td><span class="label label-sm label-dark" style="background-color: #4A4949;">Cancelado</span></td>
                      <?php } ?>
                      <td><?php echo $convenio->PLACA_VEHICULO_FK ?></td>
                      <td><?php echo $convenio->nombre?> <?php echo $convenio->apellido?></td>

                      <td>
                        <?php if($convenio->S_N_CANCELADO ==1){ ?>
                         <span>No puedes editar un convenio cancelado</span>
                      <?php }else{?>
                        <a href="?controller=convenio&method=edit&id=<?php echo $convenio->ID_CONVENIO ?>" class="btn btn-tbl-edit btn-xs"><i class="fa fa-edit"></i></a>              
                    <?php } ?>
                     </td>



                   </tr>
                 <?php endforeach ?>
               </tbody>
             </table>
           </section>
         </section>
       </main>
     </div>
   </tr>
 </thead>
 
  