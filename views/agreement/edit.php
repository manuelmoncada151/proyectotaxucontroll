<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title">Dashboard Taxucontroll

        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
            href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Nuevo Convenio</li>
        </ol>
      </div>
    </div>

    <div class="row">
     <div class="col-md-12 col-sm-12">
       <div class="card  card-box">
        <div class="card-header ">
          <h2 class="m-auto">Editar convenio</h2>
        </div>
        <div class="col-md-12" style="margin-left: 20px">
          <div class="well well-sm">
            <form class="form-horizontal" action="?controller=convenio&method=update" method="post">
              <fieldset>
                <input type="hidden" name="ID_CONVENIO" value="<?php echo $data[0]->ID_CONVENIO ?>" >
               <div class="form-group">
                 <div class="row">
                  <div class="col-md-5 ">
                   <strong>Fecha inicio convenio</strong> 
                    <div class="row">
                     <img  src="assets/icons/calendario (1).png" height="30" width="30"> 
                     <div class="col">
                      <input class="form-control" type="date" name="FECHAINICIO_CONVENIO" value="<?php echo $data[0]->FECHAINICIO_CONVENIO ?>">
                    </input>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <strong> Fecha Fin convenio</strong>
                <div class="row">
                 <img  src="assets/icons/calendario (2).png"  height="30" width="30">
                 <div class="col">
                   <input class="form-control" type="date" name="FECHAFIN_CONVENIO" value="<?php echo $data[0]->FECHAFIN_CONVENIO ?>">
                 </input>
               </div>

             </img>
           </div>
         </div>
       </div>
     </div>
      <strong> Valor producido diario</strong>
     <div class="form-group">
      <div class="row">
        <img src="assets/icons/dinero.png" height="30" width="30">
        <div class="col-md-5 ">
          <input type="number" class="form-control" required name="VALOR_CONVENIO" value="<?php echo $data[0]->VALOR_CONVENIO ?>" placeholder="Valor producido diario">
        </div>
        <img src="assets/icons/ahorro.png" height="30" width="30">
        <div class="col-md-5 ">
          <input type="number" class="form-control"  required name="AHORRO_PRODUCIDO" value="<?php echo $data[0]->AHORRO_PRODUCIDO ?>" placeholder="Valor ahorro diario">
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <img src="assets/icons/usuario.png" height="30" width="30">
        <div class="col-md-5 ">
          <select name="ID_PERSONA_FK"  required class="form-control">
            <option value="">Seleccione:</option>
              <?php
              foreach($persons as $person):{
                if ($person->ID_PERSONA == $data[0]->ID_PERSONA_FK) {
                  ?>
                  <option selected value="<?php echo$person->ID_PERSONA ?>"><?php echo $person->NOM_PERSONA ?> <?php echo $person->APE_PERSONA ?></option>
                  <?php
                }else{
                  ?><option  value="<?php echo$person->ID_PERSONA ?>"><?php echo $person->NOM_PERSONA ?> <?php echo $person->APE_PERSONA ?></option>
                  <?php
                }
              }endforeach;
              ?>
        </select>
      </div>
      <img src="assets/icons/numero.png" height="30" width="30">
      <div class="col-md-5 ">
       <select name="PLACA_VEHICULO_FK" required class="form-control">
         <?php
         foreach($cars as $car):{ 
          if ($car->PLACA_VEHICULO == $data[0]->PLACA_VEHICULO_FK) {
                  ?>
                  <option selected value="<?php echo $car->PLACA_VEHICULO ?>"><?php echo $car->PLACA_VEHICULO ?></option>
        <?php }else{ ?>
                <option value="<?php echo $car->PLACA_VEHICULO ?>"><?php echo $car->PLACA_VEHICULO ?></option>
          <?php }
          } endforeach; ?>
      </select>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <img src="assets/icons/regla.png" height="30" width="30">
    <div class="col-md-5 ">
      <select name="MEDIDA_CONVENIO" class="form-control">
        <option value="<?php echo $data[0]->MEDIDA_CONVENIO ?>"></option>
        <option value="Diario">Diario</option>
        <option value="Semanal">Semanal</option>
        <option value="Semanal">Quincenal</option>
        <option value="Mensual">Mensual</option>
      </select>
    </div>
    <img src="assets/icons/salvar.png" height="30" width="30">
    <div class="col-md-5 ">
     <button type="submit" class="btn btn-outline-warning btn-lg">Guardar</button>
   </div>
 </div>
</div>

</fieldset>
</form>
</div>
</div>
</div>
</section>
</section>

</main>
</div>

</div>
</div>
</div>


