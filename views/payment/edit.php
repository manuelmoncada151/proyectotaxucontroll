<div class="container body">
  <div class="main_container">
      <div class="right_col" >
        <main class="container">
          <section class="col-md-12 text-center">
            <section class="row mt-6">
                <div class="card-header ">
                    <h2 class="m-auto">
                        Editar pago
                    </h2>
                </div>
                <div class="col-md-12">
                    <div class="well well-sm">
                        <form class="form-horizontal" action="?controller=payment&method=update" method="post">
                            <input type="hidden" name="ID_CONVENIO_FK" value="<?php echo $convenio[0]->ID_CONVENIO ?>">
                            <input type="hidden" name="ID_PRODUCIDO" value="<?php echo $data[0]->ID_PRODUCIDO ?>">
                            <fieldset>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-2 text-center">
                                            <img height="120" src="assets/icons/dinero.png" width="120">
                                            </img>
                                        </div>
                                        <div class="form-group col-md-4">
                                            Valor a ingresar
                                            <div class="row">
                                                <img height="30" src="assets/icons/moneda.png" width="30">
                                                    <div class="col ">
                                                        <input class="form-control" name="VALOR_PRODUCIDO" value="<?php echo $data[0]->VALOR_PRODUCIDO ?>" placeholder="Valor a ingresar" type="number">
                                                        </input>
                                                    </div>
                                                </img>
                                            </div>
                                            Fecha
                                            <div class="row">
                                                <img height="30" src="assets/icons/calendario (1).png" width="30">
                                                    <div class="col">
                                                        <input class="form-control " readonly=»readonly»  name="FECHA_PRODUCIDO" value="<?php echo $data[0]->FECHA_PRODUCIDO ?>" type="text">
                                                        </input>
                                                    </div>
                                                </img>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row ">
                                        <h3>
                                            Selecione tipo de pago:
                                        </h3>
                                        <label></label>
                                    </div>
                                  
                                    
                                        <div class="col-md-6">
                                            <input id="1" selected name="S_N_PRODUCIDO" type="radio" value="1">
                                                <label for="1">
                                                    <h5>
                                                        Producido
                                                    </h5>
                                                </label>
                                            </input>
                                        
                                        <div class="col-md-6">
                                            <input id="1" name="S_N_PRODUCIDO" type="radio" value="2">
                                                <label for="1">
                                                    <h5>
                                                        Ahorro
                                                    </h5>
                                                </label>
                                            </input>
                                        </div>
                                    </div>
                                

                                <div class="form-group row col-md-6" >
                                    
                                      <img height="30" src="assets/icons/salvar.png" width="30">
                                                <div class="col-md-2 ">
                                                    <button style="width:110px; height:47px" class="btn btn-outline-warning btn-lg" type="submit">
                                                       Guardar 
                                                    </button>
                                                </div>
                                            </img>
                                    
                                        </img>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <script src="assets/js/pago.js">
        </script>