<?php
require('assets/fpdf/fpdf.php');
class PDF extends FPDF
{
// Cabecera de página

function Header()
{
    // Logo
   $this->SetFillColor(255,213,24); //Gris tenue de cada fila
    $this->SetTextColor(3,3,3); //Color del texto: Negro
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Movernos a la derecha
    $this->Cell(20);
    // Título
    $this->Cell(160,10,'Reporte de pagos',0,0,'C');
    // Salto de línea
    $this->Ln(20);

    $this->Cell(25,10,'VALOR',1,0,'C',0);
    $this->Cell(45,10,'CONDUCTOR',1,0,'C',0);
    $this->Cell(35,10,utf8_decode('ESTADO'),1,0,'C',0);
    $this->Cell(35,10,'TIPO DE PAGO',1,0,'C',0);
    $this->Cell(55,10,'FECHA PRODUCIDO',1,1,'C',0);
}
//3132858941FP
// Pie de página
function Footer()
{
    // Posición: a 1,5 cm del final
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Número de página
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
}

}

require 'views/conexion2.php';
if ($user=3) {
    $consulta = "SELECT P.ID_PRODUCIDO,TRUNCATE(p.VALOR_PRODUCIDO,0) as PAGO,p.S_N_CANCELADO,p.S_N_PRODUCIDO,P.FECHA_PRODUCIDO AS FECHA_PRODUCIDO,c.VALOR_CONVENIO AS  PRODUCIDO,c.AHORRO_PRODUCIDO AS AHORRO, P.ID_CONVENIO_FK AS CONVENIO,PE.ID_PERSONA AS PERSONA, U.ID_USUARIO AS USUARIO,CONCAT(PE.NOM_PERSONA,' ',PE.APE_PERSONA)AS NOMBRE  FROM pago_producido p 
            INNER JOIN convenio_producido C ON C.ID_CONVENIO=P.ID_CONVENIO_FK
            INNER JOIN persona PE ON PE.ID_PERSONA=C.ID_PERSONA_FK
            INNER JOIN usuario U ON U.ID_USUARIO=PE.ID_USUARIO_FK WHERE ID_USUARIO=$user ORDER BY p.ID_PRODUCIDO DESC  ";
}
if ($user=1) {
   $consulta = "SELECT P.ID_PRODUCIDO,TRUNCATE(p.VALOR_PRODUCIDO,0) as PAGO,p.S_N_CANCELADO,p.S_N_PRODUCIDO,P.FECHA_PRODUCIDO AS FECHA_PRODUCIDO,c.VALOR_CONVENIO AS  PRODUCIDO,c.AHORRO_PRODUCIDO AS AHORRO, P.ID_CONVENIO_FK AS CONVENIO,PE.ID_PERSONA AS PERSONA, U.ID_USUARIO AS USUARIO,CONCAT(PE.NOM_PERSONA,' ',PE.APE_PERSONA)AS NOMBRE  FROM pago_producido p 
            INNER JOIN convenio_producido C ON C.ID_CONVENIO=P.ID_CONVENIO_FK
            INNER JOIN persona PE ON PE.ID_PERSONA=C.ID_PERSONA_FK
            INNER JOIN usuario U ON U.ID_USUARIO=PE.ID_USUARIO_FK  ORDER BY p.ID_PRODUCIDO DESC  ";
}

$resultado = $mysqli->query($consulta);

$pdf = new PDF();

// $pdf->set_paper('letter','landscape');
$pdf -> AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(229, 229, 229);
while ($row = $resultado->fetch_assoc()) {
    $pdf->SetFillColor(255,213,24);
    $pdf->Cell(25,10,$row['PAGO'],1,0,'C',0);
    $pdf->Cell(45,10,$row['NOMBRE'],1,0,'C',0);
    if ($row ['S_N_CANCELADO']=='2') {
    $pdf->Cell(35,10,'CONFIRMADO',1,0,'C',0);
    }
    if ($row ['S_N_CANCELADO']=='1') {
    $pdf->Cell(35,10,'SIN CANCELAR',1,0,'C',0);
    }

    if ($row['S_N_PRODUCIDO']==1) {
    $pdf->Cell(35,10,'PRODUCIDO',1,0,'C',0);
    }
    if ($row['S_N_PRODUCIDO']==2) {
    $pdf->Cell(35,10,'AHORRO',1,0,'C',0);
    }

    $pdf->Cell(55,10,$row['FECHA_PRODUCIDO'],1,1,'C',0);

}

$pdf->Output();
?>