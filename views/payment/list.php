<script data-require="bootstrap@*" data-semver="3.1.1" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalCenterTitle">Nuevo Pago</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" action="?controller=payment&method=save" method="post">
          <input type="hidden" name="ID_CONVENIO_FK" value="<?php echo $convenio[0]->ID_CONVENIO ?>">
          <input type="hidden" name="NOMBRE" value="<?php echo $convenio[0]->NOMBRE ?>">
          <fieldset>
            <div class="form-group">
              <div class="row">
                <div class=" form-group col-md-6">
                  <strong>Seleccione tipo de pago</strong>
                  <div class="col ">
                    <label for="1">
                      <h5>
                        Producido
                      </h5>
                    </label>
                    <input  name="S_N_PRODUCIDO" type="radio" value="1"></input>
                  </div>
                  <div class="col ">
                    <label for="1">
                      <h5>
                        Ahorro   
                      </h5>
                    </label>
                    <input style="margin-left: 23px"  name="S_N_PRODUCIDO" type="radio" value="2"></input>
                  </div>
                </div>
                <div class="form-group col-md-6">
                 <strong>Valor a ingresar</strong>
                 <div class="row">
                  <img height="30" src="assets/icons/moneda.png" width="30">
                  <div class="col ">
                    <input class="form-control"  name="VALOR_PRODUCIDO" placeholder="Valor a ingresar" type="number">
                  </input>
                </div>
              </img>
            </div>
            Fecha
            <div class="row">
              <img height="30" src="assets/icons/calendario (1).png" width="30">
              <div class="col">
                <input class="form-control " readonly=»readonly» id="fecha" name="FECHA_PRODUCIDO" type="text">
              </input>
              <div class="modal-footer">
                <img height="30" src="assets/icons/salvar.png" width="30">
                <button type="submit" class="btn btn-outline-warning">Guardar</button>
              </div>
            </div>
          </img>
        </div>
      </div>
    </div>
  </div>
</fieldset>
</form>
</div>
</div>
</div>
</div>


<!-- Modal Ea -->

<div class="modal fade" id="ModalEa" tabindex="-1" role="dialog"  aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalCenterTitle">Petición de ahorro</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!--<form class="form-horizontal" action="?controller=ea&method=saves" method="post">-->
          <input type="hidden" name="ID_CONVENIO_FK" value="<?php echo $convenio[0]->ID_CONVENIO ?>">
          <fieldset>
            <div class="form-group">
              <div class="row">
                <div class=" form-group col-md-6">
                Fecha De Inicio
   
              <img height="30" src="assets/icons/calendario (1).png" width="30" style="margin-top:-10px">
              <div class="col">

            
                <input class="form-control " value="<?php echo $convenio[0]->FECHA ?>" id="fechac" readonly="readonly" style="margin-top:2px;">
              </input>
                  </div>
                </div>
                <div class="form-group col-md-6">
                 <strong>Valor a ingresar</strong>
                 <div class="row">
                  <img height="30" src="assets/icons/moneda.png" width="30">
                  <div class="col ">
                    <input class="form-control"  id="valor" required placeholder="Valor a ingresar" type="number" onkeypress="return soloNumeros(event)">
                  </input>
                </div>
              </img>
            </div>
            Fecha Fin
            <div class="row">
              <img height="30" src="assets/icons/calendario (1).png" width="30">
              <div class="col">
                <input class="form-control " readonly=»readonly» id="fecha2" name="FECHAFIN_CONVENIO" type="text">
              </input>
              <div class="modal-footer">
                <img height="30" src="assets/icons/salvar.png" width="30">
                <button type="submit" class="btn btn-outline-warning" onclick="saveah()">Guardar</button>
              </div>
            </div>
          </img>
        </div>
      </div>
    </div>
  </div>
</fieldset>
</form>
</div>
</div>
</div>
</div>


<?php $rol = $_SESSION['user']->ID_ROL_FK;?>
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title">Dashboard Taxucontroll
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
            href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Pagos</li>
        </ol>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8 col-sm-6">
        <div class="card card-topline-red">
          <div class="card-head">
            <header>Listado de Pagos</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
              <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group">
                  <?php 
                  if( $_SESSION['user']->ID_ROL_FK ==3){
                    ?>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Nuevo <i class="fa fa-plus"></i></button>
                    <?php 
                  }
                  ?>
                  </div>
                <div class="btn-group">
                  <?php 
                  if( $_SESSION['user']->ID_ROL_FK ==3){
                    ?>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#ModalEa">Petición de ahorro <i class="fa fa-plus"></i></button>
                    <?php 
                  }
                  ?>
               
              </div>
            </div>
              
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle"
                  data-toggle="dropdown">OPCIONES
                  <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right">
                  <li>
                    <a href="javascript:;">
                      <i class="fa fa-print"></i> Imprimir </a>
                    </li>
                    <li>
                      <a href="?controller=payment&method=pdf">
                        <i class="fa fa-file-pdf-o"></i> Exportar a PDF </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                          <i class="fa fa-file-excel-o"></i> Exportar a Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="table-wrap">
                  <div class="table-responsive tblDriverDetail">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column full-width"
                    id="example" >
                    <thead>
                      <tr>
                       <th >Valor</th>
                       <th>Fecha</th>
                       <th>Tipo de pago</th>
                       <?php if( $rol==1 ){ ?>
                         <th>Conductor</th>
                       <?php }?>
                       <?php if( $rol==3){ ?>
                         <th >Estado</th>
                       <?php } ?>
                       <?php if( $rol==1 || $rol==3){ ?>
                         <th >Acciones</th>
                       <?php } ?>
                     </tr> 
                   </thead>
                   <tbody>
                    <?php foreach ($payments as $payment): ?>
                      <tr>
                        <td>$<?php echo $payment->PAGO ?></td>
                        <td><?php echo $payment->FECHA_PRODUCIDO ?></td>
                        <?php if ($payment->S_N_PRODUCIDO == 1) {?>
                         <td>Producido</td>
                       <?php }?>
                       <?php if ($payment->S_N_PRODUCIDO == 2) {?>
                         <td>Ahorro</td>
                       <?php } if($rol == 3) {?>
                         <?php if ($payment->S_N_CANCELADO == 2) {?>
                           <td> <img src="assets/images/error.png" style="width: 20px; height: 20px"> Sin cancelar</td>
                         <?php }?>
                         <?php if ($payment->S_N_CANCELADO == 1) {?>
                           <td> <img src="assets/images/correcto.png" style="width: 20px; height: 20px"> Cancelado</td>
                         <?php } }?>
                         <?php
                         if ($rol == 1) {?>
                          <td>
                            <?php echo $payment->NOMBRE ?>
                          </td>
                          <td>
                            <!-- <a href="?controller=payment&method=edit&id=<?php echo $payment->ID_PRODUCIDO ?>" class="btn btn-primary">Editar</a> -->
                            <?php if ($payment->S_N_CANCELADO==2) { ?>
                              <button class="btn btn-tbl-edit btn-xs" data-href="?controller=payment&method=updateStatus&id=<?php echo $payment->ID_PRODUCIDO ?>" data-toggle="modal" data-target="#confirm-delete">
                                <i class="fa fa-check"></i>
                              </button>
                            <?php }else{ ?>
                              <span class="label label-sm label-dark" style="background-color: #4A4949;">Confirmado</span>
                            <?php } ?>
                          </td>
                        <?php }?>
                        <?php if ($rol == 3) {?>
                          <td>
                            <?php if ($payment->S_N_CANCELADO == 1) {?>
                              <span class="label label-sm box-shadow-1 btn btn-default">Ya no puedes eliminar este pago</span>
                            <?php }else{ ?>
                              <a href="
                              ?controller=payment&method=delete&id=<?php echo $payment->ID_PRODUCIDO ?>" class="label label-sm box-shadow-1  btn btn-danger" readonly="readonly">Eliminar</a>
                            <?php } ?>

                          <?php }?>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
                <button type="submit" class="btn btn-light" onclick="modal()">Ver estado de pagos</button>
                <div id="fecha"></div>

              </div>
            </div>
          </div>
          <div id="area_line_chart" style=""></div>
        </div>
      </div>
      <div class="col-lg-4 col-md-12 col-sm-12 col-12">
        <?php if($rol==3 ||$rol==1){ ?>
          <div class="row state-overview">
            <div class="col-lg-10 col-md-6 col-sm-6 col-12">
              <div class="info-box bg-b-purple">
                <span class="info-box-icon push-bottom"><i
                  class="fa fa-money"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text"><?php if($rol==3 ){?>Dinero ahorrado: <?php } else{echo "Ahorro general"; }?>
                      </span>
                    <span class="info-box-number"><?php if($totalahorro[0]->PRODUCIDO==null)
                    {
                      echo "No tienes dinero ahorrado.";
                    }?>
                    <?php if($totalahorro[0]->PRODUCIDO!=null) {?>$<?php echo $totalahorro[0]->PRODUCIDO;}?>
                    </span>
                    <div class="progress">
                      <div class="progress-bar width-60"></div>
                    </div>
                    <span class="progress-description">

                    </span>
                  </div>

                  <!-- /.info-box-content -->
                </div>
              </div>
            </div>
          <?php } ?>
          <div class="row state-overview">
            <div class="col-lg-10 col-md-6 col-sm-12 col-12">
              <div class="info-box bg-b-black">
                <span class="info-box-icon push-bottom"><i
                  class="fa fa-bitcoin"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text"> <div id="mes"></div> </span>
                    <span class="info-box-number"><?php if ($totalpayments[0]->PRODUCIDO!=null) {
                      ?>$<?php echo $diciembre[0]->TOTAL;
                    }?>
                    <?php if($totalpayments[0]->PRODUCIDO==null){?>$<?php echo "0";}?> 
                  </span>
                    <div class="progress">
                      <div class="progress-bar width-80"></div>
                    </div>
                    <span class="progress-description">

                    </span>
                  </div>
                  <input type="hidden" class="col-md-2"name="dias" id="dias">
                  <input type="hidden" name="ID_CONVENIO_FK" id="convenio" value="<?php echo $convenio[0]->ID_CONVENIO ?>">
                  <input type="hidden" name="ID_CONVENIO_FK" id="FECHAINICIO_CONVENIO" value="<?php echo $convenio[0]->FECHA?>">
                  <input type="hidden" class="col-md-2"name="total" id="total" value="<?php echo $totalpayments[0]->PRODUCIDO ?>">
                   <input type="hidden" class="col-md-2"name="total" id="diciembre" value="<?php echo $diciembre[0]->TOTAL ?>">
                  <input type="hidden" class="col-md-2"name="total" id="fecha" value="<?php echo $convenio[0]->FECHA ?>">
                  <input type="hidden" class="col-md-2"name="total" id="aprox" value="<?php echo $aprox[0]->Aprox ?>">
                  <input type="hidden" class="col-md-2"name="totalahorro" id="totalahorro" readonly="readonly" value="<?php echo  $totalahorro[0]->PRODUCIDO ?>">
                  <input type="hidden" class="col-md-2"name="producido" id="producido" value="<?php echo $payment->PRODUCIDO ?>">
                  <input type="hidden" class="col-md-2"name="ahorro" id="ahorro" value="<?php echo $payment->AHORRO ?>">
                   <input type="hidden" class="col-md-2"name="ahorro" id="rol" value="<?php echo $rol?>">
                  <!-- /.info-box-content -->
                </div>
              </div>
            </div>
            <input type="hidden" id="enero" value="<?php echo $enero[0]->TOTAL ?> ">
            <input type="hidden" id="febrero" value="<?php echo($febrero[0]->TOTAL); ?>">
            <input type="hidden" id="marzo" value="<?php echo($marzo[0]->TOTAL); ?>">
            <input type="hidden" id="abril" value="<?php echo($abril[0]->TOTAL); ?>">
            <input type="hidden" id="mayo" value="<?php echo($mayo[0]->TOTAL); ?>">
            <input type="hidden" id="junio" value="<?php echo($junio[0]->TOTAL); ?> ">
            <input type="hidden" id="julio" value="<?php echo($julio[0]->TOTAL); ?>">
            <input type="hidden" id="agosto" value="<?php echo($agosto[0]->TOTAL); ?> ">
            <input type="hidden" id="septiembre" value="<?php echo($septiembre[0]->TOTAL); ?>">
            <input type="hidden" id="octubre" value="<?php echo($octubre[0]->TOTAL); ?>">
            <input type="hidden" id="noviembre" value="<<?php echo($noviembre[0]->TOTAL); ?>">
            <input type="hidden" id="diciembre" value="<?php echo($diciembre[0]->TOTAL); ?>">
   
            <canvas id="GraficaPago"></canvas>
          </div>
        </div>
      </div>
    </div>
    <!--Modal-->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">

            <h3 class="modal-title" id="exampleModalLongTitle"><strong>¡Espera! ¿Estas seguro de confirmar este pago?</strong></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Si tu confirmas este pago, ya no habrá vuelta atras...</p>
            <p><small>Debes confirmar los pagos solo cuando tengas el dinero</small></p>
             
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-success btn-ok">Confirmar</a>
          </div>
        </div>
      </div>
    </div>
      <script>
      $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

        $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
      });
    </script>
  </main>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="assets/js/pago.js"></script>
  <script src="assets/js/ea.js"></script>



