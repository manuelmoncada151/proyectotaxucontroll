<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->


<!-- Mirrored from radixtouch.in/templates/templatemonster/ecab/source/light/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Feb 2020 07:02:43 GMT -->
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta name="description" content="Responsive Admin Template" />
  <meta name="author" content="SmartUniversity" />
  <title>Menú | Taxucontroll</title>
  <link rel="icon" href="assets/images/taxicon.png" type="image/ico" />
  <!-- icons -->
  <link href="assets/css/font-awesome.css" rel="stylesheet">
<!--   <link href="assets/fonts/material-icon.css" rel="stylesheet" type="text/css" /> -->
 
  <!--bootstrap -->
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/summernote.css" rel="stylesheet">
  <!-- morris chart -->
  <!-- <link href="assets/css/morris.css" rel="stylesheet" type="text/css" /> -->
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
  <!-- Material Design Lite CSS -->
  <link rel="stylesheet" href="assets/css/material.min.css">
  <link rel="stylesheet" href="assets/css/material_style.css">
  <!-- animation -->
  <link href="assets/css/animate_page.css" rel="stylesheet">
  <!-- Theme Styles -->
  <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/stylee.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="assets/css/sweetalert.min.css">
  <link rel="stylesheet" href="assets/dataTables/datatables/DataTables-1.10.21/css/dataTables.bootstrap4.min.css">
  <!-- dropzone -->
  <link href="assets/css/dropzone.css" rel="stylesheet" media="screen">
  <!-- favicon -->
  <!--  Datatables  -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>  
    <!-- searchPanes -->
    <link rel="stylesheet" href="assets/css/searchPanes.dataTables.min.css">
    <!-- select -->
    <link href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
    <style>
  table thead{
  background: linear-gradient(to right, #F9E11D, #F9E11D); 
  color:white;
  }
    </style>
</head>
<!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
  <div class="page-wrapper">
    <!-- start header -->
    <div class="page-header navbar navbar-fixed-top">
      <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo">
          <a href="index.html">
            <img alt="" src="assets/images/taxicon.png">
            <span class="logo-default">axucontroll</span> </a>
        </div>
        <!-- logo end -->
        <ul class="nav navbar-nav navbar-left in">
          <li><a href="#" class="menu-toggler sidebar-toggler font-size-23"><i class="fa fa-bars"></i></a>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-left in">
          <!-- start full screen button -->
          <li><a href="javascript:;" class="fullscreen-click font-size-20"><i
                class="fa fa-arrows-alt"></i></a></li>
          <!-- end full screen button -->
        </ul>
        <!-- start mobile menu -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
          data-target=".navbar-collapse">
          <span></span>
        </a>
        <!-- end mobile menu -->
        <!-- start header menu -->
        <div class="top-menu">
          <ul class="nav navbar-nav pull-right">
            <!-- start notification dropdown -->
            
            <!-- end notification dropdown -->
            <!-- start message dropdown -->
            
            <!-- end message dropdown -->
            <!-- start manage user dropdown -->
            <li class="dropdown dropdown-user">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                data-close-others="true">
                <img alt="" class="img-circle " src="assets/icons/taxi.png" />
              </a>
              <ul class="dropdown-menu dropdown-menu-default animated jello">
                <li>
                  <a href="?controller=home&method=index">
                    <i class="fa fa-user-o"></i> Perfil </a>
                </li>
                <li>
                  <a href="?controller=login&method=logout">
                    <i class="fa fa-sign-out"></i> Cerrar Sesión </a>
                </li>
              </ul>
            </li>
            <!-- end manage user dropdown -->
          </ul>
        </div>
      </div>
    </div>
    <!-- end header -->
    <!-- start page container -->
    <div class="page-container">
      <!-- start sidebar menu -->
      <div class="sidebar-container">
        <div class="sidemenu-container navbar-collapse collapse fixed-menu">
          <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true"
              data-slide-speed="200">
              <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                  <span></span>
                </div>
              </li>
              <li class="sidebar-user-panel">
                <div class="user-panel">
                  <div class="pull-left image">
                    <img src="assets/icons/taxi.png" class="img-circle user-img-circle"
                      alt="User Image" />
                  </div>
                  <div class="pull-left info">
                    <p>Rol: <?php echo $_SESSION['user']->role ?></p>
                    <a title="Logout" href="?controller=login&method=logout"><i
                        class="fa fa-power-off"></i></a>
                  </div>
                </div>
              </li>
              <li class="menu-heading">
                <span><?php echo $_SESSION['user']->NOM_USUARIO ?></span>
              </li>
              <?php
                if ($_SESSION['user']->ID_ROL_FK ==1) {
              ?>  
              <li class="nav-item ">
                <a href="?controller=User" class="nav-link nav-toggle">
                  <i class="fa fa-user"></i>
                  <span class="title">Usuario</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="?controller=Veh" class="nav-link nav-toggle">
                  <i class="fa fa-taxi"></i>
                  <span class="title">Vehículo</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link nav-toggle">
                  <i class="fa fa-exclamation-circle"></i>
                  <span class="title">Novedad</span>
                  <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                  <li class="nav-item">
                    <a href="?controller=novelty" class="nav-link ">
                      <span class="title">Novedades</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="?controller=Classif" class="nav-link ">
                      <span class="title">Clasificación</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="?controller=typenov" class="nav-link ">
                      <span class="title">Tipo</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item">
                <a href="?controller=convenio" class="nav-link nav-toggle">
                  <i class="fa fa-book"></i>
                  <span class="title">Convenio</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="?controller=payment" class="nav-link nav-toggle">
                  <i class="fa fa-money"></i>
                  <span class="title">Pagos</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="?controller=Ea" class="nav-link nav-toggle">
                  <i class="fa fa-envelope"></i>
                  <span class="title">Petición Ahorro</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link nav-toggle">
                  <i class="fa fa-list"></i>
                  <span class="title">Otros</span>
                  <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                  <li class="nav-item">
                    <a href="?controller=brand" class="nav-link ">
                      <span class="title">Marca</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="?controller=status" class="nav-link ">
                      <span class="title">Estado</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="?controller=role" class="nav-link ">
                      <span class="title">Rol</span>
                    </a>
                  </li>
                  <?php
                    } else  if($_SESSION['user']->ID_ROL_FK ==2){
                  ?>
                  <li class="nav-item ">
                    <a href="?controller=User" class="nav-link nav-toggle">
                      <i class="fa fa-user"></i>
                      <span class="title">Usuario</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="?controller=Veh" class="nav-link nav-toggle">
                      <i class="fa fa-taxi"></i>
                      <span class="title">Vehículo</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="?controller=novelty" class="nav-link nav-toggle">
                      <i class="fa fa-exclamation-circle"></i>
                      <span class="title">Novedad</span>
                    </a>
                  </li>
                      <?php
                    } else  if($_SESSION['user']->ID_ROL_FK ==3){
                      ?>
                      <li class="nav-item">
                        <a href="?controller=novelty" class="nav-link nav-toggle">
                          <i class="fa fa-exclamation-circle"></i>
                          <span class="title">Novedad</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="?controller=payment" class="nav-link nav-toggle">
                          <i class="fa fa-money"></i>
                          <span class="title">Pagos</span>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="?controller=Ea" class="nav-link nav-toggle">
                          <i class="fa fa-envelope"></i>
                          <span class="title">Petición Ahorro</span>
                        </a>
                      </li>
                    </li>
                  <?php
                }
                ?>
                </ul>
              </li>
                </ul>
              </li>
            </ul>
          </div>
        
      
      <!-- end sidebar menu -->
      <!-- start page content -->

          <!-- Taxi live location start -->
          <!-- Taxi live location end -->
                    <!-- /.info-box-content -->
                    <!-- /.info-box-content -->
                    <!-- /.info-box-content -->
                    <!-- /.info-box-content -->
          <!-- chart start -->

          <!-- Chart end -->
          <!-- start Payment Details -->
         

          <!-- end Payment Details -->
          
      <!-- end page content -->
      <!-- start chat sidebar -->
      <div class="chat-sidebar-container" data-close-on-body-click="false">
        <div class="chat-sidebar">
          <ul class="nav nav-tabs">
            <li class="nav-item">
              <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab">Theme
              </a>
            </li>
            <li class="nav-item">
              <a href="#quick_sidebar_tab_2" class="nav-link tab-icon" data-toggle="tab"> Settings
              </a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane chat-sidebar-settings in show active animated shake" role="tabpanel"
              id="quick_sidebar_tab_1">
              <div class="slimscroll-style">
                <div class="theme-light-dark">
                  <h6>Sidebar Theme</h6>
                  <button type="button" data-theme="white"
                    class="btn lightColor btn-outline btn-circle m-b-10 theme-button">Light
                    Sidebar</button>
                  <button type="button" data-theme="dark"
                    class="btn dark btn-outline btn-circle m-b-10 theme-button">Dark
                    Sidebar</button>
                </div>
                <div class="theme-light-dark">
                  <h6>Sidebar Color</h6>
                  <ul class="list-unstyled">
                    <li class="complete">
                      <div class="theme-color sidebar-theme">
                        <a href="#" data-theme="white"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="dark"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="blue"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="indigo"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="cyan"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="green"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="red"><span class="head"></span><span
                            class="cont"></span></a>
                      </div>
                    </li>
                  </ul>
                  <h6>Header Brand color</h6>
                  <ul class="list-unstyled">
                    <li class="theme-option">
                      <div class="theme-color logo-theme">
                        <a href="#" data-theme="logo-white"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="logo-dark"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="logo-blue"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="logo-indigo"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="logo-cyan"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="logo-green"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="logo-red"><span class="head"></span><span
                            class="cont"></span></a>
                      </div>
                    </li>
                  </ul>
                  <h6>Header color</h6>
                  <ul class="list-unstyled">
                    <li class="theme-option">
                      <div class="theme-color header-theme">
                        <a href="#" data-theme="header-white"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="header-dark"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="header-blue"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="header-indigo"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="header-cyan"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="header-green"><span class="head"></span><span
                            class="cont"></span></a>
                        <a href="#" data-theme="header-red"><span class="head"></span><span
                            class="cont"></span></a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- Start Setting Panel -->
            <div class="tab-pane chat-sidebar-settings animated slideInUp" id="quick_sidebar_tab_2">
              <div class="chat-sidebar-settings-list slimscroll-style">
                <div class="chat-header">
                  <h5 class="list-heading">Layout Settings</h5>
                </div>
                <div class="chatpane inner-content ">
                  <div class="settings-list">
                    <div class="setting-item">
                      <div class="setting-text">Sidebar Position</div>
                      <div class="setting-set">
                        <select
                          class="sidebar-pos-option form-control input-inline input-sm input-small ">
                          <option value="left" selected="selected">Left</option>
                          <option value="right">Right</option>
                        </select>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Header</div>
                      <div class="setting-set">
                        <select
                          class="page-header-option form-control input-inline input-sm input-small ">
                          <option value="fixed" selected="selected">Fixed</option>
                          <option value="default">Default</option>
                        </select>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Sidebar Menu </div>
                      <div class="setting-set">
                        <select
                          class="sidebar-menu-option form-control input-inline input-sm input-small ">
                          <option value="accordion" selected="selected">Accordion</option>
                          <option value="hover">Hover</option>
                        </select>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Footer</div>
                      <div class="setting-set">
                        <select
                          class="page-footer-option form-control input-inline input-sm input-small ">
                          <option value="fixed">Fixed</option>
                          <option value="default" selected="selected">Default</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="chat-header">
                    <h5 class="list-heading">Account Settings</h5>
                  </div>
                  <div class="settings-list">
                    <div class="setting-item">
                      <div class="setting-text">Notifications</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-1">
                            <input type="checkbox" id="switch-1" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Show Online</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-7">
                            <input type="checkbox" id="switch-7" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Status</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-2">
                            <input type="checkbox" id="switch-2" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">2 Steps Verification</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-3">
                            <input type="checkbox" id="switch-3" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-header">
                    <h5 class="list-heading">General Settings</h5>
                  </div>
                  <div class="settings-list">
                    <div class="setting-item">
                      <div class="setting-text">Location</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-4">
                            <input type="checkbox" id="switch-4" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Save Histry</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-5">
                            <input type="checkbox" id="switch-5" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="setting-item">
                      <div class="setting-text">Auto Updates</div>
                      <div class="setting-set">
                        <div class="switch">
                          <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect"
                            for="switch-6">
                            <input type="checkbox" id="switch-6" class="mdl-switch__input"
                              checked>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <!-- start footer -->
    <!-- end footer -->
  </div>
<!-- start js include path -->
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/popper.min.js"></script>
  <script src="assets/js/jquery.blockui.min.js"></script>
  <script src="assets/js/jquery.slimscroll.min.js"></script>
  <!-- bootstrap -->
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.sparkline.min.js"></script>
  <script src="assets/js/sparkline-data.js"></script>
  <!-- Common js-->
  <script src="assets/js/apps.js"></script>
  <script src="assets/js/layout.js"></script>
  <script src="assets/js/theme-color.js"></script>
  <!-- material -->
  <script src="assets/js/material.min.js"></script>
  <!-- animation -->
  <script src="assets/js/animations.js"></script>
  <!-- morris chart -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
  <!-- <script src="assets/js/morris.min.js"></script>
  <script src="assets/js/raphael-min.js"></script>
  <script src="assets/js/morris_chart_data.js"></script> -->
  <script src="assets/js/validacion.js"></script>
  <script src="assets/js/sweetalert.min.js"></script>
  <script src="assets/js/sweet-alert-data.js"></script>
  <!-- google map -->
  <script src="assets/js/modernizr.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtPIcsjNx-GEuJDPmiXOVyB3G9k1eulX0&amp;callback=initMap"
    async defer></script>
  <script src="assets/js/gmap-home.js"></script>
  <!-- dropzone -->
  <script src="assets/js/dropzone.js"></script>
  <script src="assets/js/dropzone-call.js"></script>
  
  <!-- end js include path -->
  
  <!--   Datatables-->
    <script type="text/javascript" src="assets/datatables/js/search.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

  <!-- para usar botones en datatables JS -->  
    <!-- <script src="assets/js/dataTables.buttons.min.js"></script>  
    <script src="assets/js/jszip.min.js"></script>    
    <script src="assets/js/pdfmake.min.js"></script>    
    <script src="assets/js/vfs_fonts.js"></script>
    <script src="assets/js/buttons.html5.min.js"></script> 
 -->
    <!-- searchPanes   -->
    <script src="assets/js/dataTables.searchPanes.min.js"></script>
    <!-- select -->
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    
    <script>
    $(document).ready(function(){
        $('#example').DataTable({
          "order": [[3, "desc" ]]
        });
    });

    $(document).ready(function(){
        $('#dt1').DataTable({
                searchPanes:{
                    cascadePanes:true,
                    dtOpts:{
                        dom:'tp',
                        paging:'true',
                        pagingType:'simple',
                        searching:false
                    }
                },
                dom:'Pfrtip',
                columnDefs:[{
                    searchPanes:{
                        show:false
                    },
                    targets:[0,1,3,9,5,2,6,4,10]
                }
                ]
        });

    });

//     $(document).ready(function() {    
//     $('#example2').DataTable({        
//         language: {
//                 "lengthMenu": "Mostrar _MENU_ registros",
//                 "zeroRecords": "No se encontraron resultados",
//                 "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
//                 "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
//                 "infoFiltered": "(filtrado de un total de _MAX_ registros)",
//                 "sSearch": "Buscar:",
//                 "oPaginate": {
//                     "sFirst": "Primero",
//                     "sLast":"Último",
//                     "sNext":"Siguiente",
//                     "sPrevious": "Anterior"
//            },
//            "sProcessing":"Procesando...",
//             },
//         //para usar los botones   
//         responsive: "true",
//         dom: 'Bfrtilp',       
//         buttons:[ 
//       {
//         extend:    'excelHtml5',
//         text:      '<i class="fas fa-file-excel"></i> ',
//         titleAttr: 'Exportar a Excel',
//         className: 'btn btn-success'
//       },
//       {
//         extend:    'pdfHtml5',
//         text:      '<i class="fas fa-file-pdf"></i> ',
//         titleAttr: 'Exportar a PDF',
//         className: 'btn btn-danger'
//       },
//       {
//         extend:    'print',
//         text:      '<i class="fa fa-print"></i> ',
//         titleAttr: 'Imprimir',
//         className: 'btn btn-info'
//       },
//     ]         
//     });     
// });
    </script>
</body>

<!-- Mirrored from radixtouch.in/templates/templatemonster/ecab/source/light/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Feb 2020 07:03:59 GMT -->
</html>