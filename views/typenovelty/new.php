<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Información del Tipo</h2>
			</div>

			<div class="card-body">
				<form action="?controller=TypeNov&method=save" method="post" >
					<div class="form-group">
						<label>Nombre</label>
						<input type="text" name="NOM_TIPONOV" class="form-control" onkeypress="return soloLetras(event)" required>
					</div>
					<div class="form-group">
						<label>Clasificación</label>
						<select name="ID_CLASINOV_FK" class="form-control">
							<option value="">Seleccione:</option>
							<?php
							foreach($classifs as $classif){
								if ($classif->ID_CLASINOV == $data[0]->ID_CLASINOV_FK) {
									?>
									<option selected value="<?php echo$classif->ID_CLASINOV ?>"><?php echo $classif->NOM_CLASINOV ?></option>
									<?php
								}else{
									?><option  value="<?php echo$classif->ID_CLASINOV ?>"><?php echo $classif->NOM_CLASINOV ?></option>
									<?php
								}
							}
							?>
						</select>
					</div>
					<div class="form-group">
						<button class="btn btn-primary" id="submit">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</section>
</main>
</div>
</section>
</div>
</div>