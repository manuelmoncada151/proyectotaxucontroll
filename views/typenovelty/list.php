<!--Apertura container-->
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="page-bar">
			<div class="page-title">Dashboard Taxucontroll
				<ol class="breadcrumb page-breadcrumb pull-right">
					<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
						href="?controller=home">Inicio</a>&nbsp;<i class="fa fa-angle-right"></i>
					</li>
					<li class="active">N Tipos</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card card-topline-red">
					<div class="card-head">
						<header>Listado de Tipos de Novedad</header>
						<div class="tools">
							<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
							<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
							
						</div>
					</div>
					<div class="card-body ">
						<div class="row p-b-20">
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group">
									<button type="button" class="btn btn-info"  data-toggle="modal" data-target="#exampleModal">
										Nuevo <i class="fa fa-plus"></i>
									</button>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-6">
								<div class="btn-group pull-right">
									<button class="btn deepPink-bgcolor  btn-outline dropdown-toggle"
									data-toggle="dropdown">Opciones
									<i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;">
											<i class="fa fa-print"></i> Imprimir </a>
										</li>
										<li>
											<a href="javascript:;">
												<i class="fa fa-file-pdf-o"></i> Exportar a PDF </a>
											</li>
											<li>
												<a href="javascript:;">
													<i class="fa fa-file-excel-o"></i> Exportar a Excel </a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<div class="table-responsive tblDriverDetail">
										<table class="table display product-overview mb-30" id="example" style="margin-top:50px;">
											<thead>
												<tr>
													<th>#</th>
													<th>Nombre</th>
													<th>Clasificación</th>
													<th>Acciones</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($types as $type) : ?>
													<tr>
														<td><?php echo $type->ID_TIPONOV ?></td>
														<td><?php echo $type->NOM_TIPONOV ?></td>
														<td><?php echo $type->classif ?></td>
														<td>
															<a class="btn btn-tbl-delete btn-xs" href="?controller=TypeNov&method=edit&id=<?php echo $type->ID_TIPONOV ?>"><i class="fa fa-edit "></i></a>
															<a class="btn btn-tbl-eliminar btn-xs" href="?controller=TypeNov&method=delete&id=<?php echo $type->ID_TIPONOV ?>"><i class="fa fa-trash-o "></i></a>

														</td>
													</tr>
												<?php endforeach ?> 
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Button trigger modal -->

		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Nuevo Tipo</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="card-body">
							<form action="?controller=TypeNov&method=save" method="post">
								<div class="form-group">
									<label><label id="oblig">* </label> Nombre</label>
									<input type="text" placeholder="Ej: Rayón en el auto" name="NOM_TIPONOV" class="form-control" onkeypress="return soloLetras(event)" required>
								</div>
								<div class="form-group">
									<label><label id="oblig">* </label> Clasificación</label>
									<select name="ID_CLASINOV_FK" class="form-control">
										<option value="">Seleccione:</option>
										<?php
										foreach($classifs as $classif){
											if ($classif->ID_CLASINOV == $data[0]->ID_CLASINOV_FK) {
												?>
												<option selected value="<?php echo$classif->ID_CLASINOV ?>"><?php echo $classif->NOM_CLASINOV ?></option>
												<?php
											}else{
												?><option  value="<?php echo$classif->ID_CLASINOV ?>"><?php echo $classif->NOM_CLASINOV ?></option>
												<?php
											}
										}
										?>
									</select>
								</div>
								<div class="form-group">
									<button class="btn btn-primary" id="submit">Guardar</button>
								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
