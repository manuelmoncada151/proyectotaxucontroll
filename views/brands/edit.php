<div class="container body">
	<div class="main_container">
		<section class="col-md-12 table-responsive">

			<div class="right_col" >
				<main class="container">
					<section class="col-md-12 text-center">
		<div class="card w-75 m-auto">
			<div class="card-header container">
				<h2 class="m-auto">Información de la Marca</h2>
			</div>

			<div class="card-body">
				<form action="?controller=brand&method=update" method="post">
					<input type="hidden" name="ID_MARCA" value="<?php echo $data[0]->ID_MARCA ?>" >
				
					<div class="form-group">
						<label>Estado</label>
						<input type="text" name="NOM_MARCA" class="form-control" placeholder="Ingrese Marca" value="<?php echo $data[0]->NOM_MARCA ?>">
					</div>
					
					<div class="form-group">
						<button class="btn btn-primary">Actualizar</button>
					</div>
				</form>			
			</div>
		</div>
	</section>
</main>