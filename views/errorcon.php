<!DOCTYPE html>
<html lang="en">

<head>
	  <link href="assets/css/error.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div>
  <aside><img src="assets/images/error.png" alt="404 Image" />
  </aside>
  <main>
  	<form action="?controlle=home" method="POST">
    <h1>Lo sentimos!</h1>
    <p>
      La contraseña ingresada no puede estar vacia <em>. . . Son cuestiones de seguridad.</em>
    </p>
    <button>Intentarlo de nuevo</button>
  </main>
</form>
</div>

<script src="assets/js/animacion.js"></script>
</body>
</html>