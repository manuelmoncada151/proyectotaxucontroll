<?php
	

	require 'models/Role.php';

	class RoleController
	{
		private $model;
		
		public function __construct()
		{
			$this->model = new Role;
			
		}

		public function index() 
		{
			$rol=$_SESSION['user']->ID_ROL_FK;
		 if ($rol==1) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
            $roles = $this->model->getAll();
			require 'views/role/list.php';
			require 'views/footer.php';
		}
		if ($rol==2) {
			header('Location: ?controller=home');
			}
		if ($rol==3) {
			header('Location: ?controller=home');
		}
			
		}
		
		public function add() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
			require 'views/role/list.php';
			}else{
			header('Location: ?controller=login');
		}
		}

		public function save()
		{
			$this->model->newRole($_REQUEST);			
			header('Location: ?controller=role');
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_SESSION['user'])){
			if(isset($_REQUEST['id'])) {
				$ID_ROL = $_REQUEST['id'];
				$data = $this->model->getRoleById($ID_ROL);
				require 'views/layout.php';
				require 'views/role/edit.php';
			} else {
				echo "Error";
			}
			}else{
			header('Location: ?controller=login');
		}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editRole($_POST);			
				header('Location: ?controller=role');				
			} else {
				echo "Error";
			}
		}

		public function delete()
		{			
			$this->model->deleteRole($_REQUEST);		
			header('Location: ?controller=role');
		}

	}