<?php
	

	require 'models/Classif.php';

	class ClassifController
	{
		private $model;
		
		public function __construct()
		{
			$this->model = new Classif;
			
		}

		public function index() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
            $classifs = $this->model->getAll();
			require 'views/classif/list.php';
			require 'views/footer.php';

			}else{
			header('Location: ?controller=login');
		}
			
		}
		
		public function add() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
			require 'views/classif/new.php';
			}else{
			header('Location: ?controller=login');
		}
		}

		public function save()
		{
			$this->model->newClassif($_REQUEST);			
			header('Location: ?controller=classif');
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_SESSION['user'])){
			if(isset($_REQUEST['id'])) {
				$ID_CLASINOV = $_REQUEST['id'];
				$data = $this->model->getClassifById($ID_CLASINOV);
				require 'views/layout.php';
				require 'views/classif/edit.php';
			} else {
				echo "Error";
			}
			}else{
			header('Location: ?controller=login');
		}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editClassif($_POST);			
				header('Location: ?controller=classif');				
			} else {
				echo "Error";
			}
		}

		public function delete()
		{			
			$this->model->deleteClassif($_REQUEST);		
			header('Location: ?controller=classif');
		}

	}