<?php
	

	require 'models/Status.php';

	class StatusController
	{
		private $model;
		
		public function __construct()
		{
			$this->model = new Status;
			
		}

		public function index() 
		{
		$rol=$_SESSION['user']->ID_ROL_FK;
		 if ($rol==1) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
            $statuses = $this->model->getAll();
			require 'views/status/list.php';
			require 'views/footer.php';
				}
		if ($rol==2) {
			header('Location: ?controller=home');
			}
		if ($rol==3) {
			header('Location: ?controller=home');
		}
		
			
		}
		
		public function add() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
			require 'views/status/list.php';
			}else{
			header('Location: ?controller=login');
		}
		}

		public function save()
		{
			$this->model->newStatus($_REQUEST);			
			header('Location: ?controller=status');
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_SESSION['user'])){
			if(isset($_REQUEST['id'])) {
				$ID_ESTADO = $_REQUEST['id'];
				$data = $this->model->getStatusById($ID_ESTADO);
				require 'views/layout.php';
				require 'views/status/edit.php';
			} else {
				echo "Error";
			}
			}else{
			header('Location: ?controller=login');
		}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editStatus($_POST);			
				header('Location: ?controller=status');				
			} else {
				echo "Error";
			}
		}

		public function delete()
		{			
			$this->model->deleteStatus($_REQUEST);		
			header('Location: ?controller=status');
		}

	}