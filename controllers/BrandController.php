<?php
	
	/**
	 * Clase UserController
	 */

	require 'models/Brands.php';
	

	class BrandController
	{
		private $model;
	
		
		public function __construct()
		{
			$this->model = new brand;
	
		}

		public function index() 
		{
		$rol=$_SESSION['user']->ID_ROL_FK;
		 if ($rol==1) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$brands = $this->model->getAll();
			require 'views/brands/list.php';
		}
		if ($rol==2) {
			header('Location: ?controller=home');
			}
		if ($rol==3) {
			header('Location: ?controller=home');
		}
		}

		//muestra la vista de crear
		public function add() 
		{
		
			require 'views/layout.php';
			require 'views/brands/new.php';
			
		}

		// Realiza el proceso de guardar
		public function save()
		{
			$this->model->newBrand($_REQUEST);			
			header('Location: ?controller=brand');
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_REQUEST['id'])) {
			
				$ID_MARCA = $_REQUEST['id'];
				$data = $this->model->getBrandById($ID_MARCA);
		
				require 'views/layout.php';
				require 'views/brands/edit.php';
				
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editBrand($_POST);			
				header('Location: ?controller=brand');				
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de borrar
		public function delete()
		{			
			$this->model->deleteBrand($_REQUEST);		
			header('Location: ?controller=brand');
		}
	}