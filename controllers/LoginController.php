<?php
	
	require 'models/Login.php';
	/**
	 * Clase
	 */
	class LoginController
	{		
		private $model;

		public function __construct()
		{
			$this->model = new Login;
		}

		public function index()
		{
			if(isset($_SESSION['user'])){
				header('Location: ?controller=home');
			}else{
				require "views/login.php";
			}
		}

		public function login()
		{
			$validateUser = $this->model->validateUser($_POST);
			if ($validateUser === true) {
				header('Location: ?controller=home');
			} else {
				$error = [
					'errorMessage' => $validateUser,
					'nombre' => $_POST['nombre']
				];
				require "views/login.php";
			}
			
		}

		public function logout()
		{
			if(isset($_SESSION['user'])){
				session_destroy();
				require "index.php";
			}else{
				require "index.php";
			}
			
		}
	}
?>