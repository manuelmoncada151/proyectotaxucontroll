<?php
	
	/**
	 * Clase UserController
	 */

	require 'models/DetNovelty.php';
	require 'models/Novelty.php';
	require 'models/TypeNov.php';

	class DetNoveltyController
	{
		private $model;
		private $type;
		private $novelty;
		
		public function __construct()
		{
			$this->model = new DetNovelty;
			$this->type = new TypeNov;
			$this->novelty = new Novelty;
		}

		public function index() 
		{
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$detnoveltys = $this->model->getAll();
			require 'views/detnovelty/list.php';
			require 'views/footer.php';
		}

		//muestra la vista de editar
		public function listar()
		{
			if(isset($_REQUEST['id'])) {
				$ID_DETNOV = $_REQUEST['id'];
				$data = $this->model->getAll($ID_DETNOV);
				$detnoveltys = $this->model->getAll();
				require 'views/layout.php';
				require 'views/detnovelty/list.php';
				require 'views/footer.php';
			} else {
				echo "Error";
			}
		}

	}