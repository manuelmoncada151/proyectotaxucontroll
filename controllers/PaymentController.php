<?php
	
	require 'models/Payment.php';
	require 'models/Convenio.php';
	require 'models/Login.php';
	class PaymentController
	{
		private $model;
		private $convenio;
		public function __construct()
		{
			$this->model = new Payment;
			$this->convenio = new Convenio;
			$this->login = new Login;
		}

		public function index()
		{ 
		$rol=$_SESSION['user']->ID_ROL_FK;
		 if ($rol==1) {
			require 'views/layout.php';
			$enero=$this->model->getTotalEnero();
			$febrero=$this->model->getTotalFebrero();
			$marzo=$this->model->getTotalMarzo();
			$abril=$this->model->getTotalAbril();
			$mayo=$this->model->getTotalMayo();
			$junio=$this->model->getTotalJunio();
			$julio=$this->model->getTotalJulio();
			$agosto=$this->model->getTotalAgosto();
			$septiembre=$this->model->getTotalSeptiembre();
			$octubre=$this->model->getTotalOctubre();
			$noviembre=$this->model->getTotalNoviembre();
			$diciembre=$this->model->getTotalDiciembre();
			$payments= $this->model->getAllById();
			$totalpayments= $this->model->getTotalPaymentById();
			$totalahorro= $this->model->getTotalAhorroById();
			$convenio = $this->convenio->getConvenioUser();
			$payments= $this->model->getAll();
			$aprox= $this->model->getAprox2();
			$totalpayments= $this->model->getTotalPayment();
			$totalahorro= $this->model->getTotalAhorro();
			require 'views/payment/list.php';
			}
		if ($rol==2) {
			header('Location: ?controller=home');
			}
		if ($rol==3) {
			$convenio = $this->convenio->getConvenioUser();
			$diciembre=$this->model->getTotalDiciembreById($convenio);
			$aprox= $this->model->getAprox();
			require 'views/layout.php';
			$payments= $this->model->getAllById();
			$totalpayments= $this->model->getTotalPaymentById();
			$totalahorro= $this->model->getTotalAhorroById();
			$convenio = $this->convenio->getConvenioUser();
			require 'views/payment/list.php';
			}
		

		}
		public function add() 
		{
					require 'views/layout.php';
					$convenio = $this->convenio->getConvenioUser();
					require 'views/payment/new.php';
		}
		public function save() 
		{
			$admins=$this->login->getAdmin();
			$arrnotify=[$_POST["NOMBRE"],$_POST["VALOR_PRODUCIDO"]];
			$this->login->Notificacion($arrnotify,$admins);
			$payment=[
				$_POST["ID_CONVENIO_FK"],
				$_POST["S_N_PRODUCIDO"],
				$_POST["VALOR_PRODUCIDO"],
				$_POST["FECHA_PRODUCIDO"]
			];
			$this->model->newPayment($payment);			
			header('Location: ?controller=payment');
				
		}
		public function pdf() 
		{
					$persona=$this->model->getAllById();
					$user = $_SESSION['user']->ID_USUARIO;
					require 'views/payment/pdfpayment.php';

		}

		public function edit() 
		{
			if(isset($_REQUEST['id'])) {
				$id = $_REQUEST['id'];
				$convenio = $this->convenio->getConvenioUser();
				$data = $this->model->getPaymentById($id);
				require 'views/layout.php';
				require 'views/payment/edit.php';
			} else {
				echo "Error";
			}
				
		}

		public function update()
		{
			if(isset($_POST)) {
				$this->model->editPayment($_POST);			
				header('Location: ?controller=payment');			
			} else {
				echo "Error";
			}
		}
		public function updateStatus()
		{

			try {
				$payment= $this->model->getPaymentById($_REQUEST['id']);
				$data=[];
				if($payment[0]->S_N_CANCELADO==2){

					$data = [
						'ID_PRODUCIDO'=> $payment[0]->ID_PRODUCIDO,
						'S_N_CANCELADO'=>1
					];
				}elseif ($payment[0]->S_N_CANCELADO==1) {
					$data = [
						'ID_PRODUCIDO'=> $payment[0]->ID_PRODUCIDO,
						'S_N_CANCELADO'=>1
					];
							
			}$this->model->editPaymentStatus($data);
					header('Location:?controller=payment');
			} catch (Exception $e) {
				echo "Error";
			}
		}
		public function delete()
		{
			$this->model->deletePayment($_REQUEST);		
			header('Location: ?controller=payment');
		}

 	}
 ?>