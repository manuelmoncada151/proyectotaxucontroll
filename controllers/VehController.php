<?php
	
	/**
	 * Clase UserController
	 */

	require 'models/Vehicle.php';
	require 'models/Status.php';
	require 'models/Person.php';
	require 'models/Brands.php';


	class VehController
	{
		private $model;
		private $status;
		private $person;
		private $brand;

		
		public function __construct()
		{
			$this->model = new Veh;
			$this->status = new Status;
			$this->person = new Person;
			$this->brand = new Brand;
		}

		public function index() 
		{
			 $rol=$_SESSION['user']->ID_ROL_FK;
		 if ($rol==1) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$vehs = $this->model->getAll();
			require 'views/vehicles/list.php';
			require 'views/footer.php';
			}
		if ($rol==2) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$vehs = $this->model->getAll();
			require 'views/vehicles/list.php';
			require 'views/footer.php';
			}
			if ($rol==3) {
			header('Location: ?controller=home');
			}
		}
	

		//muestra la vista de crear
		public function add() 
		{
			
			
			require 'views/layout.php';
			$persons = $this->person->getAll();
			$brands = $this->brand->getAll();
			require 'views/vehicles/new.php';
			
		}
			
		

		// Realiza el proceso de guardar
		//public function save()
		//{
		
			//$confirm = $this->model->newVeh($_REQUEST);
						
			//header('Location: ?controller=veh');
		//}
				public function save()
		{
			$dataVeh = [
            'PLACA_VEHICULO' => $_POST['PLACA_VEHICULO'],
            'MODELO_VEHICLO' => $_POST['MODELO_VEHICLO'],
            'FECHAING_VEHICULO' => $_POST['FECHAING_VEHICULO'],
            'ID_MARCA_FK' => $_POST['ID_MARCA_FK'],
            'ID_PERSONA_FK' => $_POST['ID_PERSONA_FK'],
            'ID_ESTADO_FK' => 1
        ]; 

   


            //inserción Pelicula
            $respVeh = $this->model->newVeh($dataVeh);
         
            //Obtener El ultimo Id registrado de la tabla movie
            $lastIdVeh = $this->model->getLastId();

         
  
            $respVeh = true;
    


        //validar si las inserciones se realizaron correctamente
        $arrayResp = [];

        if($respVeh == true ) {
            $arrayResp = [
                'success' => true,
                'message' =>  "Vehiculo Creada"
            ];
        } else {
            $arrayResp = [
                'error' => false,
                'message' => "Error Creando la Vehiculo"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_REQUEST['id'])) {
				
				$ID_VEHICULO= $_REQUEST['id'];
				$data = $this->model->getVehById($ID_VEHICULO);
	
				$persons = $this->person->getAll();
				$brands = $this->brand->getAll();

				require 'views/layout.php';
				require 'views/vehicles/edit.php';
				 
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editVeh($_POST);			
				header('Location: ?controller=veh');				
			} else {
				echo "Error";
			}
		}

		// Realiza el proceso de borrar
		public function delete()
		{			
			$this->model->deleteVeh($_REQUEST);		
			header('Location: ?controller=veh');
		}

		public function updateStatus()
		{
			$veh = $this->model->getVehById($_REQUEST['id']);
			$data = [];
			if($veh[0]->ID_ESTADO_FK == 1){
			$data =['ID_VEHICULO'=> $veh[0]->ID_VEHICULO,'ID_ESTADO_FK' =>2];
				} elseif($veh[0]->ID_ESTADO_FK== 2) {
					$data = ['ID_VEHICULO'=> $veh[0]->ID_VEHICULO,'ID_ESTADO_FK' => 1];

					}
					$this->model->editVeh($data);
					header('Location: ?controller=veh');
				
			}
	}