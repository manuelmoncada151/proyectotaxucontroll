<?php

	/**
	 * Clase UserController
	 */

	require 'models/Person.php';
	require 'models/Classif.php';
	require 'models/Vehicle.php';
	require 'models/Novelty.php';
	require 'models/Status.php';
	require 'models/TypeNov.php';

	class NoveltyController
	{
		private $model;
		private $status;
		private $person;
		private $veh;
		private $classif;
		private $type;
		
		public function __construct()
		{
			$this->model = new Novelty;
			$this->status = new Status;
			$this->person = new Person;
			$this->veh = new Veh;
			$this->classif = new Classif;
			$this->type = new TypeNov;
			
		}

		public function index() 
		{
			if(isset($_SESSION['user'])){
			$rol=$_SESSION['user']->ID_ROL_FK;
		 if ($rol==1) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$noveltys = $this->model->getAll();
			$detnoveltys = $this->model->getAll();
			require 'views/novelty/list.php';
			require 'views/footer.php';
			}
		if ($rol==2) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$noveltys = $this->model->getAll();
			require 'views/novelty/list.php';
			require 'views/footer.php';
			}
		if ($rol==3) {
			require 'views/layout.php';
			//Llamado al metodo que trae todos los usuarios
			$noveltys = $this->model->getAllById();
			require 'views/novelty/list.php';
			require 'views/footer.php';
			}
			}else{
			header('Location: ?controller=login');
		}


		}

		//muestra la vista de crear
		public function add() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
			$noveltys = $this->model->getAllById();
			$vehs = $this->model->getVeh();
			$classifs = $this->classif->getAll();
			$types = $this->type->getAll();
			$persons = $this->person->getActivePersons();
			require 'views/novelty/new.php';
			}else{
			header('Location: ?controller=login');
		}


		}

		// Realiza el proceso de guardar
		public function save()
		{
			$dataNovelty = [
            'DESCRIP_NOVEDAD' => $_POST['DESCRIP_NOVEDAD'],
            'FECH_NOVEDAD' => $_POST['FECH_NOVEDAD'],
            'URL_SUPNOV' => $_POST['URL_SUPNOV'],
            'VALOR_NOVEDAD' => $_POST['VALOR_NOVEDAD'],
            'ID_PERSONA_FK' => $_POST['ID_PERSONA_FK'],
            'ID_CLASINOV_FK' => $_POST['ID_CLASINOV_FK'],
            'ID_ESTADO_FK' => 3
        ]; 

        $dataVeh = [
            'ID_ESTADO_FK' => $_POST['ID_ESTADO_FK'],
            'vehiculo' => $_POST['vehiculo']
           
        ]; 

 
        //variable con array de categorias que llegan desde el Frontend
        $arrayTypes = isset($_POST['types']) ? $_POST['types'] : [];

        if(!empty($arrayTypes)) {

            //inserción Pelicula
            $respNovelty = $this->model->newNovelty($dataNovelty);
            $respVeh = $this->model->UpdateVeh($dataVeh);
            //Obtener El ultimo Id registrado de la tabla movie
            $lastIdNovelty = $this->model->getLastId();

            //Inserción a la tabla category_movie
            $repsDetNovelty = $this->model->saveDetNovelty($arrayTypes, $lastIdNovelty[0]->id);
        } else {
            $respNovelty = false;
            $repsDetNovelty = false;
            $respVeh = false;
        }


        //validar si las inserciones se realizaron correctamente
        $arrayResp = [];

        if($respNovelty == true && $repsDetNovelty == true) {
            $arrayResp = [
                'success' => true,
                'message' =>  "Novedad Creada"
            ];
        } else {
            $arrayResp = [
                'error' => false,
                'message' => "Error Creando la Novedad"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editNovelty($_POST);			
				header('Location: ?controller=novelty');				
			} else {
				echo "Error";
			}
		}

		public function updateStatus()
		{
			$novelty = $this->model->getNoveltyById($_REQUEST['id']);
			$data = [];
			if($novelty[0]->ID_ESTADO_FK == 3){
				$data = [
					'ID_NOVEDAD' => $novelty[0]->ID_NOVEDAD,
					'ID_ESTADO_FK' => 4
				];	

				
			}elseif($novelty[0]->ID_ESTADO_FK == 4){

				$data = [
					'ID_NOVEDAD' => $novelty[0]->ID_NOVEDAD,
					'ID_ESTADO_FK' => 3
				];	

				
			}
			$this->model->editStatus($data);
			header('Location: ?controller=novelty');
		}

		public function updateStatusVeh()
		{
			$veh = $this->model->getVehById($_REQUEST['id']);
			$data = [];
			if($veh[0]->ID_ESTADO_FK == 1){

				$data =[
					'ID_VEHICULO'=> $veh[0]->ID_VEHICULO,
					'ID_ESTADO_FK' =>2];

				} elseif($veh[0]->ID_ESTADO_FK== 2) {
					$data = [
						'ID_VEHICULO'=> $veh[0]->ID_VEHICULO,
						'ID_ESTADO_FK' => 1];

					}
					$this->model->editVeh($data);
					header('Location: ?controller=novelty');
		}


	}
	// array(1) { [0]=> object(stdClass)#19 (10) { ["ID_CONVENIO"]=> string(1) "3" ["FECHAINICIO_CONVENIO"]=> string(10) "2019-09-02" ["FECHAFIN_CONVENIO"]=> string(10) "2024-04-03" ["MEDIDA_CONVENIO"]=> string(7) "Semanal" ["VALOR_CONVENIO"]=> string(8) "90000.00" ["AHORRO_PRODUCIDO"]=> string(7) "5000.00" ["S_N_CANCELADO"]=> string(1) "1" ["PLACA_VEHICULO_FK"]=> string(7) "678-ABC" ["ID_PERSONA_FK"]=> string(1) "5" ["vehiculo"]=> string(7) "678-ABC" } }
