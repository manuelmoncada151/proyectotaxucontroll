<?php
	

	require 'models/TypeNov.php';
	require 'models/Classif.php';

	class TypeNovController
	{
		private $model;
		private $classif;
		
		public function __construct()
		{
			$this->model = new TypeNov;
			$this->classif = new Classif;
			
		}

		public function index() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
			//Llamado al metodo que trae todos los tipos de novedad
            $types = $this->model->getAll();
            $classifs = $this->classif->getAll();
			require 'views/typenovelty/list.php';
			require 'views/footer.php';
			}else{
			header('Location: ?controller=login');
		}
		}

		public function add() 
		{
			if(isset($_SESSION['user'])){
			require 'views/layout.php';
			$classifs = $this->classif->getAll();
			}else{
			header('Location: ?controller=login');
		}


		}

		public function save()
		{
			$this->model->newTypeNov($_REQUEST);			
			header('Location: ?controller=TypeNov');
		}

		//muestra la vista de editar
		public function edit()
		{
			if(isset($_SESSION['user'])){
			if(isset($_REQUEST['id'])) {
				$ID_TIPONOV = $_REQUEST['id'];
				$data = $this->model->getTypeNovById($ID_TIPONOV);
				$classifs = $this->classif->getAll();
				require 'views/layout.php';
				require 'views/typenovelty/edit.php';
			} else {
				echo "Error";
			}
			}else{
			header('Location: ?controller=login');
		}
		}

		// Realiza el proceso de actualizar
		public function update()
		{
			if(isset($_POST)) {
				$this->model->editTypeNov($_POST);			
				header('Location: ?controller=typenov');				
			} else {
				echo "Error";
			}
		}

		public function delete()
		{			
			$this->model->deleteTypeNov($_REQUEST);		
			header('Location: ?controller=typenov');
		}

	}