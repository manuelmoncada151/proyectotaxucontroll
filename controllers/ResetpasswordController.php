<?php
require 'models/login.php';
	class ResetpasswordController
	{

		private $model;

		public function __construct()
		{
			$this->model = new Login;
		}

		public function validateuser() 
		{	
			$correo=$_POST['correo'];
			$nombre=$_POST['name'];
			$email=$this->model->validateEmail($correo);
			if ($email===true) {
				$sendpassword=$this->model->sendpassword();
				$info=[$correo,$sendpassword];
				$updatepassword=$this->model->updatepassword($info);
				if ($updatepassword===true) {
					require "views/login.php";?>
					<script type="text/javascript">
					swal("Se ha enviado la nueva contraseña a <?php echo ($correo) ?>",
						"por favor revisa tu bandeja de entrada","success");
					</script>
					<?php
				}
			}else{
				require "views/login.php";
				?>
				<script type="text/javascript">
					swal("El correo electronico <?php echo ($correo) ?>",
						"no esta registrado en nuestra base de datos si desconoces el correo contactate con el administrador","error");
				</script>
				<?php
			}
		}
	}
	?>