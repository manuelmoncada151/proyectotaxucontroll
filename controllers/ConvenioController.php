<?php
	
	require 'models/Convenio.php';
	require 'models/Status.php';
	require 'models/Person.php';
	require 'models/Vehicle.php';

	class ConvenioController
	{
		private $model;
		private $status;
		private $person;
		private $vehicle;

		public function __construct()
		{
			$this->model = new Convenio;
			$this->person = new Person;
			$this->vehicle = new Veh;
			
		}

		public function index()
		{  
			require 'views/layout.php';
			$conductores=$this->model->Count();
			$convenios= $this->model->getAll();
			$promedio = $this->model->promedio();
			require 'views/agreement/list.php';
		}
		public function add() 
		{
					require 'views/layout.php';
					$persons = $this->person->getActivePersons();
					$cars = $this->vehicle->getAll();
					require 'views/agreement/new.php';
				
		}
		public function save() 
		{

			$this->model->newConvenio($_REQUEST);			
			header('Location: ?controller=convenio');
				
		}

		public function edit() 
		{
			if(isset($_REQUEST['id'])) {
				$id = $_REQUEST['id'];
				$data = $this->model->getConvenioById($id);
				$persons = $this->person->getActivePersons();
				require 'views/layout.php';
				require 'views/agreement/edit.php';
			} else {
				echo "Error";
			}
				
		}

		public function update()
		{
			if(isset($_POST)) {
				$this->model->editConvenio($_POST);			
				header('Location: ?controller=convenio');			
			} else {
				echo "Error";
			}
		}
		public function delete()
		{
			$this->model->deleteConvenio($_REQUEST);		
			header('Location: ?controller=convenio');
		}

 	}
 ?>